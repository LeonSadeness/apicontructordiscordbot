# Some comment
FROM mcr.microsoft.com/dotnet/core/sdk:2.2
ADD src /src
WORKDIR /src
COPY src app/src
RUN  dotnet restore
RUN  dotnet build /src/RedPandaDiscordBot -c Release
CMD [ "dotnet", "/src/RedPandaDiscordBot/bin/Release/netcoreapp2.1/RedPandaDiscordBot.dll" ]