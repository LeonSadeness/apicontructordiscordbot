﻿using DSharpPlus;
using Newtonsoft.Json;
using RedPandaDiscordBot.CBuilder.EventArgs;
using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.Interfaces;
using RedPandaDiscordBot.Services;
using System;

namespace RedPandaDiscordBot.CBuilder
{
    /// <summary>
    /// Базовый билдер для комманд
    /// </summary>
    public class BaseCommandBuilder : ICommandBuilder
    {
        /// <summary>
        /// Ошибки сборки комманд
        /// </summary>
        public event AsyncEventHandler<BuilderEventArgs> ErrorEvent
        {
            add { _error.Register(value); }
            remove { _error.Unregister(value); }
        }
        private AsyncEvent<BuilderEventArgs> _error;

        /// <summary>
        /// Версия билдера
        /// </summary>
        public string Version => "1.0";

        public BaseCommandBuilder()
        {
            _error = new AsyncEvent<BuilderEventArgs>(LoggerPanda.EventLogHandler, "COMMAND_BUILDER_" + Version);
        }

        /// <summary>
        /// Сборка команды из Json строки
        /// </summary>
        /// <param name="json">Json строка, содержащая одну команду</param>
        /// <returns>Возращает собранную команду или null, при ошибке сборки</returns>
        public Command BuildFromJson(string json)
        {
            Command cmd = null;
            try
            {
                cmd = JsonConvert.DeserializeObject<Command>(json);
            }
            catch (Exception e)
            {
                _error?.InvokeAsync(new BuilderEventArgs(e.Message));
            }
            return cmd;
        }
    }
}