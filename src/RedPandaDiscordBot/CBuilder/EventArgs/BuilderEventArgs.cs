﻿namespace RedPandaDiscordBot.CBuilder.EventArgs
{
    public class BuilderEventArgs : System.EventArgs
    {
        public string Msg { get; private set; }

        public BuilderEventArgs(string msg) : base() { Msg = msg; }
    }
}