﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.CPandaModule.TypeConverters;
using System;

namespace RedPandaDiscordBot.CPandaModule.Cmd.Blocks
{
    /// <summary>
    /// Аргумент команды для хранения констант пользователя,
    /// которые используются в течении выполнения всей команды
    /// </summary>
    public class BlockDefaultArgument
    {
        /// <summary>
        /// Пользовательское имя аргумента
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Значение константы
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Тип значения константы
        /// </summary>
        public string TypeValue { get; set; }
    }
}