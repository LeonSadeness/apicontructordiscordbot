﻿namespace RedPandaDiscordBot.CPandaModule.Cmd.Blocks
{
    /// <summary>
    /// Тип блока по признаку операции
    /// </summary>
    public enum BlockType
    {
        OPERATION,
        IF,
        CYCLE,
    }
}