﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RedPandaDiscordBot.CPandaModule.Cmd.Blocks
{
    /// <summary>
    /// Блок операции.
    /// Хранит в себе информацию о типе блока и вложенных блоках.
    /// Является звеном в цепочке, выполняемых командой, операций 
    /// </summary>
    public class CommandBlock
    {
        /// <summary>
        /// Тип операции в блоке
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public BlockType BlockTypeF { get; set; }

        /// <summary>
        /// Разделитель для операции IF.
        /// Указывает на индекс ячейки в массиве internalBlocks,
        /// с которого начинается выполнение операций при негативном результате IF
        /// </summary>
        public int IfCount { get; set; }

        /// <summary>
        /// Имя функции, для сборщика команд
        /// </summary>
        public string FunctionName
        {
            get => _functionName;
            set
            {
                _functionName = value;
                FunctionProvider.GetMainMethodFunc(value, out Function);
            }
        }
        private string _functionName;

        /// <summary>
        /// Функция, которая выполняет основное действие в этом блоке
        /// </summary>
        [JsonIgnore]
        private Func<CommandContext, object[], object[]> Function;

        /// <summary>
        /// Вложенные блоки, для реализации операций IF, CYCLE, или для упорядочивания
        /// </summary>
        public CommandBlock[] InternalBlocks { get; set; }

        /// <summary>
        /// Адреса переменных которые используются основной функцией этого блока.
        /// Порядок ввода  и тип должны соблюдаться
        /// </summary>
        public string[] ArgsAddresses { get; set; }

        /// <summary>
        /// Точка входа в основную функцию этого блока
        /// </summary>
        /// <param name="ctx"></param>
        public void Execute(CommandContext ctx)
        {
            object[] args = ctx.GetArgsFromAddress(ArgsAddresses);

            switch (BlockTypeF)
            {
                case BlockType.OPERATION:
                    if (Function != null)
                        ctx.ResultResiver[ctx.Command.GetMyAddress(this)] = Function(ctx, args);
                    else if (InternalBlocks != null) foreach (CommandBlock block in InternalBlocks) block.Execute(ctx);
                    break;
                case BlockType.IF:
                    if (ArgsAddresses?.Length > 0)
                    {
                        // лимит для True ветки
                        var lim = (IfCount > InternalBlocks.Length) ? InternalBlocks.Length : IfCount;
                        // условие
                        if (args[0] is bool condition)
                        {
                            if (condition)
                                for (int i = 0; i < lim; i++)
                                    InternalBlocks[i].Execute(ctx);
                            else
                                for (int i = IfCount; i < InternalBlocks.Length; i++)
                                    InternalBlocks[i].Execute(ctx);
                        }
                        else throw new Exception("Аргумент условия для блока IF отсутствует");
                    }
                    else throw new Exception("Отсутствует условие");
                    break;
                case BlockType.CYCLE:
                    if (Function != null)
                    {
                        while ((bool)Function(ctx, args)[0])
                            foreach (CommandBlock block in InternalBlocks) block.Execute(ctx);
                        break;
                    }
                    else throw new Exception("Отсутствует функция сравнения");
            }
        }
    }
}