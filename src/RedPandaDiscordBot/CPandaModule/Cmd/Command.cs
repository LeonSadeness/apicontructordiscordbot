﻿using Newtonsoft.Json;
using RedPandaDiscordBot.CPandaModule.Cmd.Blocks;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedPandaDiscordBot.CPandaModule.Cmd
{
    /// <summary>
    /// Модель исполняемой команды
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Версия команды
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Получает имя этой команды.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает описание этой команды.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Получает псевдонимы этой команды.
        /// </summary>
        public IReadOnlyList<string> Aliases { get; set; }

        /// <summary>
        /// Получает, скрыта ли эта команда.
        /// </summary>
        public bool IsHidden { get; set; }

        ///// <summary>
        ///// Получает коллекцию проверок перед выполнением этой команды.
        ///// </summary>
        //public IReadOnlyList<CheckBaseAttribute> ExecutionChecks { get; internal set; }

        /// <summary>
        /// Получает аргументы этой команды.
        /// </summary>
        public IReadOnlyList<CommandArgument> Arguments { get; set; }

        /// <summary>
        /// Константы команды.
        /// </summary>
        public BlockDefaultArgument[] Constants { get; set; }

        /// <summary>
        /// Блок функций, последовательность которых нужно выполнить.
        /// Представляет родительский блок со вложенными операциями.
        /// </summary>
        public CommandBlock Blocks { get; set; }

        /// <summary>
        /// Выполняет эту команду с указанным контекстом.
        /// </summary>
        /// <param name="ctx">Контекст для выполнения команды</param>
        /// <returns>Результаты выполнения команды.</returns>
        public virtual async Task<CommandResult> ExecuteAsync(CommandContext ctx)
        {
            try
            {
                await Task.Run(() => Blocks.Execute(ctx));
            }
            catch (Exception ex)
            {
                return new CommandResult
                {
                    IsSuccessful = false,
                    Exception = ex,
                    Context = ctx
                };
            }

            return new CommandResult
            {
                IsSuccessful = true,
                Context = ctx
            };
        }

        /// <summary>
        /// Выдает адрес блока изходя из позиции в иерархии блоков
        /// </summary>
        /// <param name="block">Блок чей адрес нужно найти</param>
        /// <returns>Возращает адрес в виде строки (0.12.23.45. ...) где каждая цифра индекс в массиве вложенных блоков начиная с блока в команде</returns>
        public string GetMyAddress(CommandBlock block)
        {

            if (Blocks == block)
                return "0";
            else
            {
                string findB(CommandBlock b)
                {
                    string a = "";
                    for (int i = 0; i < b.InternalBlocks?.Length; i++)
                    {
                        if (b.InternalBlocks[i] == block) return a = "." + i;
                        a = findB(b.InternalBlocks[i]);
                        if (a != "") return "." + i + a;
                    }
                    return a;
                }
                return "0" + findB(Blocks);
            }
        }

        ///// <summary>
        ///// Запускает предварительные проверки для этой команды и возвращает все, что не удалось для данного контекста.
        ///// </summary>
        ///// <param name="ctx">Контекст, в котором выполняется команда.</param>
        ///// <param name="help">Независимо от того, выполняется эта проверка с помощью или нет. Это можно использовать для проверки возможности выполнения команды без отключения определенных условий сбоя (например, перезарядки).</param>
        ///// <returns>Проверки перед выполнением, которые терпят неудачу для данного контекста.</returns>
        //public async Task<IEnumerable<CheckBaseAttribute>> RunChecksAsync(CommandContext ctx, bool help)
        //{
        //    var fchecks = new List<CheckBaseAttribute>();
        //    if (this.ExecutionChecks != null && this.ExecutionChecks.Any())
        //        foreach (var ec in this.ExecutionChecks)
        //            if (!(await ec.CanExecute(ctx, help)))
        //                fchecks.Add(ec);

        //    return fchecks;
        //}

        /// <summary>
        /// Проверяет, равна ли эта команда другой.
        /// </summary>
        /// <param name="cmd1">Команда для сравнения.</param>
        /// <param name="cmd2">Команда для сравнения.</param>
        /// <returns>Будь две команды равны.</returns>
        public static bool operator ==(Command cmd1, Command cmd2)
        {
            var o1 = cmd1 as object;
            var o2 = cmd2 as object;

            if (o1 == null && o2 != null)
                return false;
            else if (o1 != null && o2 == null)
                return false;
            else if (o1 == null && o2 == null)
                return true;

            return cmd1.Name == cmd2.Name;
        }

        /// <summary>
        /// Проверяет, не равна ли эта команда другой.
        /// </summary>
        /// <param name="cmd1">Команда для сравнения.</param>
        /// <param name="cmd2">Команда для сравнения.</param>
        /// <returns>Будь две команды не равны.</returns>
        public static bool operator !=(Command cmd1, Command cmd2) =>
            !(cmd1 == cmd2);

        /// <summary>
        /// Проверяет, равна ли эта команда другому объекту.
        /// </summary>
        /// <param name="obj">Объект для сравнения.</param>
        /// <returns>Является ли эта команда равной другому объекту.</returns>
        public override bool Equals(object obj)
        {
            var o1 = obj as object;
            var o2 = this as object;

            if (o1 == null && o2 != null)
                return false;
            else if (o1 != null && o2 == null)
                return false;
            else if (o1 == null && o2 == null)
                return true;

            var cmd = obj as Command;
            if ((object)cmd == null)
                return false;

            return cmd.Name == this.Name;
        }

        /// <summary>
        /// Получает хэш-код этой команды.
        /// </summary>
        /// <returns>Хэш-код этой команды.</returns>
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        /// <summary>
        /// Возвращает строковое представление этой команды.
        /// </summary>
        /// <returns>Строковое представление этой команды.</returns>
        public override string ToString()
        {
            return $"Command: {Name}";
        }
    }
}