﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedPandaDiscordBot.CPandaModule.Cmd
{
    /// <summary>
    /// Аргумент комманды
    /// </summary>
    public sealed class CommandArgument
    {
        /// <summary>
        /// Получает имя этого аргумента.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Получает тип этого аргумента.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Является ли аргумент массивом
        /// </summary>
        public bool IsArray { get => _isArray ; set => _isArray = value? (IsCatchAll=true): false; }
        private bool _isArray;

        /// <summary>
        /// Получает, является ли этот аргумент необязательным.
        /// </summary>
        public bool IsOptional { get; set; }

        /// <summary>
        /// Получает, имеет ли этот аргумент значение по умолчанию.
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// Получает, перехватывает ли этот аргумент все оставшиеся аргументы.
        /// </summary>
        public bool IsCatchAll { get; set; }

        /// <summary>
        /// Получает описание этого аргумента.
        /// </summary>
        public string Description { get; set; }
    }
}
