﻿using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RedPandaDiscordBot.CBuilder.EventArgs;
using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.Cmd.Blocks;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.CPandaModule.Exceptions;
using RedPandaDiscordBot.CPandaModule.Extensions;
using RedPandaDiscordBot.CPandaModule.Interfaces;
using RedPandaDiscordBot.Functions.PrivateFunc;
using RedPandaDiscordBot.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedPandaDiscordBot.CPandaModule
{
    public class CommandPandaModule : BaseModule
    {
        #region Events

        /// <summary>
        /// Срабатывает всякий раз, когда команда выполняется успешно.
        /// </summary>
        public event AsyncEventHandler<CommandExecutionEventArgs> CommandExecuted
        {
            add { _executed.Register(value); }
            remove { _executed.Unregister(value); }
        }
        private AsyncEvent<CommandExecutionEventArgs> _executed;

        /// <summary>
        /// Срабатывает всякий раз, когда команда выдает исключение во время выполнения.
        /// </summary>
        public event AsyncEventHandler<CommandErrorEventArgs> CommandErrored
        {
            add { _error.Register(value); }
            remove { _error.Unregister(value); }
        }
        private AsyncEvent<CommandErrorEventArgs> _error;

        #endregion

        internal CommandsPandaConfiguration Config { get; }

        public CommandPandaModule(CommandsPandaConfiguration config)
        {
            Config = config;
            _registeredCommands = new Dictionary<string, Command>();
        }

        protected override void Setup(DiscordClient client)
        {
            if (Client != null)
                throw new InvalidOperationException("Ручное использование метода Setup класса CommanPandaModule запрешщено");
            Client = client;
            Client.MessageCreated += HandleCommandsAsync;

            _executed = new AsyncEvent<CommandExecutionEventArgs>(LoggerPanda.EventLogHandler, "COMMAND_EXECUTED");
            _error = new AsyncEvent<CommandErrorEventArgs>(LoggerPanda.EventLogHandler, "COMMAND_ERRORED");

            if (Config.Builders == null && Config.Builders.Count == 0)
                throw new Exception("Отсутствуют билдеры команд");

            HelpCommandBuilder();
        }

        #region Command Handler

        public async Task HandleCommandsAsync(MessageCreateEventArgs e)
        {
            await Task.Yield();

            // пишет нам бот х*й ему в рот
            if (e.Author.IsBot)
                return;

            // если сообщение приватно, проверяем разрешение на ответ и шлем нахер, если разрешения нет
            if (!Config.EnablePM && e.Channel.IsPrivate)
                return;

            // если запрещено, не отвечаем никому кроме своей учетки
            if (Config.SelfBot && e.Author.Id != Client.CurrentUser.Id)
                return;

            // смотрим упоминания бота
            int mpos = -1;
            if (Config.EnableMentionPrefix)
                mpos = e.Message.PrefixExist(Client.CurrentUser);

            // смотрим пользовательский префикс
            if (mpos == -1 && !string.IsNullOrWhiteSpace(Config.StringPrefix))
                mpos = e.Message.PrefixExist(Config.StringPrefix);

            // некому отвечать - ну и ладно
            if (mpos == -1)
                return;

            // получаем сообщение после преффиксов
            string cnt = e.Message.Content.Substring(mpos);
            // находим имя команды
            string cms = CommandPandaUtlities.ExtractNextArgument(cnt, out var rrg);

            // ищем команду в нашем словаре
            var cmd = _registeredCommands.ContainsKey(cms) ? _registeredCommands[cms] : null;
            // если не нашли проверяем еще раз без учета регистра (если игнорирование регистра включено)
            if (cmd == null && !Config.CaseSensitive)
                cmd = _registeredCommands.FirstOrDefault(xkvp => xkvp.Key.ToLowerInvariant() == cms.ToLowerInvariant()).Value;

            // формируем контекст команды
            CommandContext ctx = new CommandContext
            {
                Client = this.Client,
                Command = cmd,
                Message = e.Message,
                RawArgumentString = rrg,
                CommandsPanda = this,
            };

            // если команду в словаре так и не нашли, досвидули
            if (cmd == null)
            {
                await _error.InvokeAsync(new CommandErrorEventArgs { Context = ctx, Exception = new CommandNotFoundException(cms) });
                return;
            }

            // выполнение найденной команды
            _ = Task.Run(async () =>
            {
                try
                {
                    // Провека аргументов перед выполнением, нужно будет обязательно добавить!
                    //var fchecks = await cmd.RunChecksAsync(ctx, false);
                    //if (fchecks.Any())
                    //    throw new ChecksFailedException(cmd, ctx, fchecks);

                    var res = await cmd.ExecuteAsync(ctx);

                    if (res.IsSuccessful)
                        await _executed.InvokeAsync(new CommandExecutionEventArgs { Context = res.Context });
                    else
                        await _error.InvokeAsync(new CommandErrorEventArgs { Context = res.Context, Exception = res.Exception });
                }
                catch (Exception ex)
                {
                    await _error.InvokeAsync(new CommandErrorEventArgs { Context = ctx, Exception = ex });
                }
            });
        }

        #endregion

        #region Command Registration

        private Dictionary<string, Command> _registeredCommands { get; set; }
        public IReadOnlyDictionary<string, Command> RegisteredCommands => _registeredCommands;

        /// <summary>
        /// Регистрация команд
        /// </summary>
        /// <param name="command">Команда для регистрации</param>
        /// <returns>Успех операции</returns>
        public bool RegisterCommands(Command command, out string log)
        {
            log = "";
            if (_registeredCommands.ContainsKey(command.Name) || (command.Aliases != null && command.Aliases.Any(xs => _registeredCommands.ContainsKey(xs))))
            {
                var dublicate = new DuplicateCommandException(command.Name);
                _error.InvokeAsync(new CommandErrorEventArgs() { Exception = dublicate });
                log += dublicate.Message;
                return false;
            }

            _registeredCommands[command.Name] = command;

            if (command.Aliases != null)
                foreach (var xs in command.Aliases)
                    _registeredCommands[xs] = command;

            return true;
        }

        /// <summary>
        /// Регистрация команд
        /// </summary>
        /// <param name="command">Команда для регистрации</param>
        /// <returns>Успех операции</returns>
        public bool RegisterCommands(Command command)
        {
            if (_registeredCommands.ContainsKey(command.Name) || (command.Aliases != null && command.Aliases.Any(xs => _registeredCommands.ContainsKey(xs))))
            {
                var dublicate = new DuplicateCommandException(command.Name);
                _error.InvokeAsync(new CommandErrorEventArgs() { Exception = dublicate });
                return false;
            }

            _registeredCommands[command.Name.ToLower()] = command;

            if (command.Aliases != null)
                foreach (var xs in command.Aliases)
                    _registeredCommands[xs] = command;

            return true;
        }

        /// <summary>
        /// Дерегистрация комманд
        /// </summary>
        /// <param name="command">Команда для удаления</param>
        /// <param name="log">Лог выполнения дерегистрации</param>
        /// <returns></returns>
        public bool UnregisteredCommand(Command command, out string log)
        {
            if(command.Name.ToLower() == "help" || (command.Aliases != null && command.Aliases.Any(a => _registeredCommands.ContainsKey(a))))
            {
                log = $"Команду Help удалять нельзя";
                return false;
            }
            if (_registeredCommands.ContainsValue(command))
            {
                if(command.Aliases?.Count > 0)
                    foreach (var item in command.Aliases)
                        _registeredCommands.Remove(item);
                _registeredCommands.Remove(command.Name);
                log = "";
                return true;
            }
            log = $"Команда с именем \"{command.Name}\" не найдена.";
            return false;
        }

        #endregion

        #region Build

        /// <summary>
        /// Собирает команды из Json строки
        /// </summary>
        /// <param name="jsonCommands">Json строка содержащая команды для сборки</param>
        /// <param name="log">Логгирование результата. Возращает пустую строку при успехе</param>
        /// <returns>Возращает собранные команды, или пустой массив при неудаче</returns>
        public List<Command> BuildCommands(string jsonCommands, out string log)
        {
            log = "";
            JToken jToken = JToken.Parse(jsonCommands);
            JArray jArray = new JArray();
            List<Command> cmds = new List<Command>();

            if (jToken is JArray ja)
            {
                jArray = ja;
            }
            else if (jToken is JObject)
            {
                jArray.Add(jToken);
            }
            else
            {
                log = $"Тип данных не определен.{Environment.NewLine}";
                return cmds;
            }

            for (int i = 0; i < jArray.Count; i++)
            {
                string ilog = "";

                if (((JObject)jArray[i]).TryGetValue("Version", out JToken value) && ((JObject)jArray[i]).TryGetValue("Name", out JToken name))
                {
                    var builder = Config.Builders.FirstOrDefault(b => b.Version == value.ToString());
                    if (builder != null)
                    {
                        builder.ErrorEvent += (e) => Task.Run(() => ilog += $"Элемент {i}({name}) - {e.Msg}{Environment.NewLine}");
                        Command cmd = builder.BuildFromJson(jArray[i].ToString());
                        builder.ErrorEvent -= (e) => Task.Run(() => ilog += $"Элемент {i}({name}) - {e.Msg}{Environment.NewLine}");

                        if (cmd != null) cmds.Add(cmd);
                    }
                    else ilog += $"Элемент {i}({name}) - Билдер для команды версии {value} не найден.{Environment.NewLine}";
                }
                else ilog += $"Элемент {i} - Не найден параметр \"Version\" в json строке.{Environment.NewLine}";
                log += ilog;
            }
            if (cmds.Count == 0) log += $"Ни одна команда не собрана.{Environment.NewLine}";
            if (!string.IsNullOrEmpty(log)) _error.InvokeAsync(new CommandErrorEventArgs() { Exception = new Exception(log) });
            return cmds;
        }

        /// <summary>
        /// Собирает команды из Json строки
        /// </summary>
        /// <param name="jsonCommands">Json строка содержащая команды для сборки</param>
        /// <returns>Возращает собранные команды, или пустой массив при неудаче</returns>
        public List<Command> BuildCommands(string jsonCommands)
        {
            string log = "";
            JToken jToken = JToken.Parse(jsonCommands);
            JArray jArray = new JArray();
            List<Command> cmds = new List<Command>();

            if (jToken is JArray ja)
            {
                jArray = ja;
            }
            else if (jToken is JObject)
            {
                jArray.Add(jToken);
            }
            else
            {
                log = "Тип данных не определен.";
                return cmds;
            }

            for (int i = 0; i < jArray.Count; i++)
            {
                string ilog = "";

                if (((JObject)jArray[i]).TryGetValue("Version", out JToken value))
                {
                    var builder = Config.Builders.FirstOrDefault(b => b.Version == value.ToString());
                    if (builder != null)
                    {
                        builder.ErrorEvent += (e) => Task.Run(() => ilog += $"Элемент {i} - {e.Msg}{Environment.NewLine}");
                        Command cmd = builder.BuildFromJson(jArray[i].ToString());
                        builder.ErrorEvent -= (e) => Task.Run(() => ilog += $"Элемент {i} - {e.Msg}{Environment.NewLine}");

                        if (cmd != null) cmds.Add(cmd);
                    }
                    else ilog += $"Элемент {i} - Билдер для команды версии {value} не найден.{Environment.NewLine}";
                }
                else ilog = $"Элемент {i} - Не найден параметр \"Version\" в json строке.{Environment.NewLine}";
                log += ilog;
            }
            if (cmds.Count == 0) log = $"Ни одна команда не собрана.{Environment.NewLine}";
            if (!string.IsNullOrEmpty(log)) _error.InvokeAsync(new CommandErrorEventArgs() { Exception = new Exception(log) });
            return cmds;
        }

        #endregion

        /// <summary>
        /// Создает и регистрирует стандартную команду Help.
        /// </summary>
        public void HelpCommandBuilder()
        {
            Command cmd_help = new Command
            {
                Version = "1.0",
                Name = "help",
                Description = "Комманда получения справки",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        Name = "Имя команды",
                        Description = "Содержит имя или псевдоним команды по которой нужна справка",
                        Type = "string",
                        IsOptional = true
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            FunctionName = "FuncDefaultHelp",
                            BlockTypeF = BlockType.OPERATION,
                            ArgsAddresses = new string[] { "0 0" }
                        },
                        new CommandBlock
                        {
                            FunctionName = "FuncSendChannelMessage",
                            BlockTypeF = BlockType.OPERATION,
                            ArgsAddresses = new string[] { null, null , "2 0.0 0" }
                        }
                    }
                }
            };

            RegisterCommands(cmd_help);
        }
    }
}