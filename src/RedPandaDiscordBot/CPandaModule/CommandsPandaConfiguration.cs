﻿using RedPandaDiscordBot.CPandaModule.Interfaces;
using System.Collections.Generic;

namespace RedPandaDiscordBot.CPandaModule
{
    /// <summary>
    /// Представляет конфигурацию для <see cref="CommandPandaModule"/>.
    /// </summary>
    public class CommandsPandaConfiguration
    {
        /// <summary>
        /// Устанавливает префикс строки, используемый для команд. По умолчанию не имеет значения.
        /// </summary>
        public string StringPrefix { internal get; set; } = null;

        /// <summary>
        ///Устанавливает, следует ли упоминать бота в качестве префикса команды. По умолчанию true.
        /// </summary>
        public bool EnableMentionPrefix { internal get; set; } = true;

        /// <summary>
        /// Устанавливает, должен ли бот отвечать только на сообщения из своей учетной записи. Это используется для автоботов. По умолчанию false.
        /// </summary>
        public bool SelfBot { internal get; set; } = false;

        /// <summary>
        /// Устанавливает, должны ли команды быть чувствительными к регистру. По умолчанию false.
        /// </summary>
        public bool CaseSensitive { internal get; set; } = false;

        /// <summary>
        /// Устанавливает, включить ли команду помощи по умолчанию. Отключите это, если вы хотите сделать свою собственную команду помощи. По умолчанию true.
        /// </summary>
        public bool EnableDefaultHelp { internal get; set; } = true;

        /// <summary>
        /// Устанавливает, включать ли команды через прямые сообщения. По умолчанию true.
        /// </summary>
        public bool EnablePM { internal get; set; } = true;

        /// <summary>
        /// Получает, следует ли игнорировать любые дополнительные аргументы, передаваемые командам. 
        /// Если для этого параметра установлено значение false, дополнительные аргументы будут выброшены. По умолчанию true.
        /// </summary>
        public bool IgnoreExtraArguments { internal get; set; } = true;

        /// <summary>
        /// Доступные билдеры
        /// </summary>
        public List<ICommandBuilder> Builders { get; set; }
    }
}