﻿using DSharpPlus;
using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.Extensions;
using RedPandaDiscordBot.CPandaModule.TypeConverters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RedPandaDiscordBot.CPandaModule.EventArgs
{
    /// <summary>
    /// Представляет контекст, в котором выполняется команда.
    /// </summary>
    public sealed class CommandContext
    {
        /// <summary>
        /// Получает клиент, получивший сообщение.
        /// </summary>
        public DiscordClient Client { get; internal set; }

        /// <summary>
        /// Получает сообщение, которое инициировало выполнение.
        /// </summary>
        public DiscordMessage Message { get; internal set; }

        /// <summary>
        /// Получает канал, в котором было запущено выполнение
        /// </summary>
        public DiscordChannel Channel => Message.Channel;

        /// <summary>
        /// Получает гильдию, в которой было выполнено выполнение. Это свойство является нулевым для команд, 
        /// отправляемых через прямые сообщения.
        /// </summary>
        public DiscordGuild Guild => Channel.Guild;

        /// <summary>
        /// Получает пользователя, который запустил выполнение.
        /// </summary>
        public DiscordUser User => Message.Author;

        internal CommandContext()
        {
            _lazy_ass_member = new Lazy<DiscordMember>(() => Guild?.Members.FirstOrDefault(xm => xm.Id == User.Id) ?? Guild?.GetMemberAsync(User.Id).GetAwaiter().GetResult());
        }

        /// <summary>
        /// Получает участника, который инициировал выполнение. Это свойство является нулевым для команд, отправляемых
        /// через прямые сообщения.
        /// </summary>
        public DiscordMember Member => _lazy_ass_member.Value;
        private Lazy<DiscordMember> _lazy_ass_member;

        /// <summary>
        /// Получает экземпляр службы CommandPandaModule, который обработал эту команду.
        /// </summary>
        public CommandPandaModule CommandsPanda { get; internal set; }

        /// <summary>
        /// Получает команду, которая выполняется.
        /// </summary>
        public Command Command { get; internal set; }

        /// <summary>
        /// Получает необработанную строку аргумента, переданную команде.
        /// </summary>
        public string RawArgumentString { get; internal set; }

        private object[] _args;
        /// <summary>
        /// Содержит сконвертированные аргументы необработанной строки
        /// </summary>
        public object[] Args { get => _args ?? (_args = CommandPandaUtlities.BindArguments(this, CommandsPanda.Config.IgnoreExtraArguments)); }

        private object[] _constants;
        /// <summary>
        /// Представляет сконвертированые константы команды
        /// </summary>
        public object[] Constants { get => _constants ?? (_constants = Command.Constants.Select((i) => i.Value.ConvertArgument(this, i.TypeValue)).ToArray()); }

        /// <summary>
        /// Содержит результаты выполения функций в виде массива обьектов.
        /// И предоставляет доступ по адресу блока.
        /// </summary>
        public Dictionary<string, object[]> ResultResiver { get; internal set; } = new Dictionary<string, object[]>();

        /// <summary>
        /// Возвращает аргументы из списков командных аргументов, констант команды, и результатов команды (GAFA)
        /// </summary>
        /// <param name="adresses">Адреса переменных</param>
        /// <returns>Массив сконвертированных аргументов</returns>
        public object[] GetArgsFromAddress(string[] adresses)
        {
            if (adresses == null) return null;

            List<object> result = new List<object>();
            foreach (string adress in adresses)
            {
                if (adress == null)
                {
                    result.Add(null);
                    continue;
                }

                string[] indexes = adress.Split(' ');

                switch (int.Parse(indexes[0]))
                {
                    case 0:
                        result.Add(Args[int.Parse(indexes[1])]);
                        break;
                    case 1:
                        result.Add(Constants[int.Parse(indexes[1])]);
                        break;
                    case 2:
                        result.Add(ResultResiver[indexes[1]][int.Parse(indexes[2])]);
                        break;
                }
            }
            return result.ToArray();
        }

        /// <summary>
        /// Быстро ответить на сообщение, которое вызвало команду.
        /// </summary>
        /// <param name="content">Сообщение для ответа.</param>
        /// <param name="is_tts">Должно ли сообщение быть произнесено вслух.</param>
        /// <param name="embed">Вставить, чтобы прикрепить к сообщению.</param>
        /// <returns></returns>
        public Task<DiscordMessage> RespondAsync(string content = null, bool is_tts = false, DiscordEmbed embed = null) =>
            Message.RespondAsync(content, is_tts, embed);

        /// <summary>
        /// Быстро ответьте файлом на сообщение, которое вызвало команду.
        /// </summary>
        /// <param name="file_data">Поток, содержащий данные для вложения в виде файла.</param>
        /// <param name="file_name">Имя файла для отправки.</param>
        /// <param name="content">Сообщение для ответа.</param>
        /// <param name="is_tts">Следует ли произносить сообщение вслух.</param>
        /// <param name="embed">Вставить, чтобы прикрепить к сообщению.</param>
        /// <returns>Сообщение было отправлено!</returns>
        public Task<DiscordMessage> RespondWithFileAsync(Stream file_data, string file_name, string content = null, bool is_tts = false, DiscordEmbed embed = null) =>
            Message.RespondWithFileAsync(file_data, file_name, content, is_tts, embed);

#if !NETSTANDARD1_1
        /// <summary>
        /// Быстро ответьте файлом на сообщение, которое вызвало команду.
        /// </summary>
        /// <param name="file_data">Поток, содержащий данные для вложения в виде файла.</param>
        /// <param name="content">Сообщение для ответа.</param>
        /// <param name="is_tts">Должно ли сообщение быть произнесено вслух.</param>
        /// <param name="embed">Вставить, чтобы прикрепить к сообщению.</param>
        /// <returns>Сообщение было отправлено!</returns>
        public Task<DiscordMessage> RespondWithFileAsync(FileStream file_data, string content = null, bool is_tts = false, DiscordEmbed embed = null) =>
            Message.RespondWithFileAsync(file_data, content, is_tts, embed);

        /// <summary>
        /// Быстро ответьте файлом на сообщение, которое вызвало команду.
        /// </summary>
        /// <param name="file_path">Путь к файлу, который будет прикреплен к сообщению.</param>
        /// <param name="content">Сообщение для ответа.</param>
        /// <param name="is_tts">Должно ли сообщение быть произнесено вслух.</param>
        /// <param name="embed">Вставить, чтобы прикрепить к сообщению.</param>
        /// <returns>Сообщение было отправлено!</returns>
        public Task<DiscordMessage> RespondWithFileAsync(string file_path, string content = null, bool is_tts = false, DiscordEmbed embed = null)
        {
            return Message.RespondWithFileAsync(file_path, content, is_tts, embed);
        }
#endif

        /// <summary>
        /// Быстро ответить несколькими файлами на сообщение, которое вызвало команду.
        /// </summary>
        /// <param name="content">Сообщение для ответа.</param>
        /// <param name="files">Файлы для отправки.</param>
        /// <param name="is_tts">Должно ли сообщение быть произнесено вслух.</param>
        /// <param name="embed">Вставить, чтобы прикрепить к сообщению.</param>
        /// <returns>Сообщение было отправлено!</returns>
        public Task<DiscordMessage> RespondWithFilesAsync(Dictionary<string, Stream> files, string content = null, bool is_tts = false, DiscordEmbed embed = null) =>
            Message.RespondWithFilesAsync(files, content, is_tts, embed);

        /// <summary>
        /// Запускает ввод в канале, содержащем сообщение, которое вызвало команду.
        /// </summary>
        /// <returns></returns>
        public Task TriggerTypingAsync() =>
            Channel.TriggerTypingAsync();
    }
}