﻿using System;

namespace RedPandaDiscordBot.CPandaModule.EventArgs
{
    /// <summary>
    /// Предоставляет аргументы для <see cref="CommandPandaModule.CommandErrored"/> event.
    /// </summary>
    public class CommandErrorEventArgs : CommandEventArgs
    {
        public Exception Exception { get; internal set; }
    }
}