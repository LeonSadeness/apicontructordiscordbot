﻿using RedPandaDiscordBot.CPandaModule.Cmd;

namespace RedPandaDiscordBot.CPandaModule.EventArgs
{
    /// <summary>
    /// Базовый класс для всех событий, связанных с CommandPanda.
    /// </summary>
    public class CommandEventArgs : System.EventArgs
    {
        /// <summary>
        /// Получает контекст, в котором была выполнена команда.
        /// </summary>
        public CommandContext Context { get; internal set; }

        /// <summary>
        /// Получает команду, которая была выполнена.
        /// </summary>
        public Command Command => this.Context.Command;
    }
}