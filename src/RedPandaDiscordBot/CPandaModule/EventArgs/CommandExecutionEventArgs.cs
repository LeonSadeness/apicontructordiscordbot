﻿namespace RedPandaDiscordBot.CPandaModule.EventArgs
{
    /// <summary>
    ///  Представляет аргументы для <see cref="CommandPandaModule.CommandExecuted"/> event.
    /// </summary>
    public class CommandExecutionEventArgs : CommandEventArgs
    {
    }
}