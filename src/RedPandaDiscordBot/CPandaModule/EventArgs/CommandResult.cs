﻿using System;

namespace RedPandaDiscordBot.CPandaModule.EventArgs
{
    /// <summary>
    /// Представляет результат выполнения команды.
    /// </summary>
    public struct CommandResult
    {
        /// <summary>
        /// Получает, удалось ли выполнить команду.
        /// </summary>
        public bool IsSuccessful { get; internal set; }

        /// <summary>
        /// Получает исключение (если есть), возникшее при выполнении команды.
        /// </summary>
        public Exception Exception { get; internal set; }

        /// <summary>
        /// Получает контекст, в котором была выполнена команда.
        /// </summary>
        public CommandContext Context { get; internal set; }
    }
}