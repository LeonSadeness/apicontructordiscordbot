﻿using System;

namespace RedPandaDiscordBot.CPandaModule.Exceptions
{
    /// <summary>
    /// Выдается, когда службе команд не удается найти команду.
    /// </summary>
    public sealed class CommandNotFoundException : Exception
    {
        /// <summary>
        /// Получает имя команды, которая не была найдена.
        /// </summary>
        public string Command { get; set; }

        /// <summary>
        /// Создает новый <see cref="CommandNotFoundException"/>.
        /// </summary>
        /// <param name="command">Ненайденная команда</param>
        public CommandNotFoundException(string command)
            : base("Указанная команда не найдена.")
        {
            Command = command;
        }
    }
}
