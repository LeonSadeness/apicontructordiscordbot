﻿using System;

namespace RedPandaDiscordBot.CPandaModule.Exceptions
{
    /// <summary>
    /// Указывает, что данное имя команды или псевдоним приняты.
    /// </summary>
    public class DuplicateCommandException : Exception
    {
        /// <summary>
        /// Получает имя команды, которая уже существует.
        /// </summary>
        public string CommandName { get; }

        /// <summary>
        /// Создает новое исключение, указывающее, что данное имя команды уже занято.
        /// </summary>
        /// <param name="name">Name of the command that was taken.</param>
        public DuplicateCommandException(string name) : base("Команда с данным именем уже существует.")
        {
            CommandName = name;
        }
    }
}
