﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.CPandaModule.TypeConverters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RedPandaDiscordBot.CPandaModule.Extensions
{
    public static class CommandPandaUtlities
    {
        static Regex UserRegex = new Regex(@"<@\!?(\d+?)> ", RegexOptions.ECMAScript);

        /// <summary>
        /// Проверяет, имеет ли сообщение указанный префикс строки.
        /// </summary>
        /// <param name="msg">Сообщение для проверки</param>
        /// <param name="str">Искомый префикс</param>
        /// <returns>Положительное число, если префикс присутствует, -1 в противном случае.</returns>
        public static int PrefixExist(this DiscordMessage msg, string str)
        {
            string cnt = msg.Content;
            if (str.Length >= cnt.Length)
                return -1;

            if (!cnt.StartsWith(str))
                return -1;

            int sn = 0;
            for (var i = str.Length; i < cnt.Length; i++)
                if (char.IsWhiteSpace(cnt[i]))
                    sn++;
                else
                    break;

            return str.Length + sn;
        }

        /// <summary>
        /// Проверяет, содержит ли сообщение указанный префикс упоминания.
        /// </summary>
        /// <param name="msg">Сообщение для проверки</param>
        /// <param name="user">Искомый пользователь</param>
        /// <returns>Положительное число, если префикс присутствует, -1 в противном случае.</returns>
        public static int PrefixExist(this DiscordMessage msg, DiscordUser user)
        {
            string cnt = msg.Content;
            if (!cnt.StartsWith("<@"))
                return -1;

            int cni = cnt.IndexOf('>');
            if (cni == -1 || cnt.Length <= cni + 2)
                return -1;

            string cnp = cnt.Substring(0, cni + 2);
            Match m = UserRegex.Match(cnp);
            if (!m.Success)
                return -1;

            ulong uid = ulong.Parse(m.Groups[1].Value, CultureInfo.InvariantCulture);
            if (user.Id != uid)
                return -1;

            int sn = 0;
            for (int i = m.Value.Length; i < cnt.Length; i++)
                if (char.IsWhiteSpace(cnt[i]))
                    sn++;
                else
                    break;

            return m.Value.Length + sn;
        }

        #region ArgumentsUtilities

        internal static string ExtractNextArgument(string str, out string remainder)
        {
            remainder = null;
            if (string.IsNullOrWhiteSpace(str))
                return null;

            bool in_backtick = false;
            bool in_triple_backtick = false;
            bool in_quote = false;
            bool in_escape = false;

            int i = 0;
            for (; i < str.Length; i++)
                if (!char.IsWhiteSpace(str[i]))
                    break;
            if (i > 0)
                str = str.Substring(i);
            int x = i;

            int ep = -1;
            for (i = 0; i < str.Length; i++)
            {
                if (char.IsWhiteSpace(str[i]) && !in_quote && !in_triple_backtick && !in_backtick && !in_escape)
                    ep = i;

                if (str[i] == '\\')
                {
                    if (!in_escape && !in_backtick && !in_triple_backtick)
                    {
                        in_escape = true;
                        if (str.IndexOf("\\`", i) == i || str.IndexOf("\\\"", i) == i || str.IndexOf("\\\\", i) == i || (str.Length >= i && char.IsWhiteSpace(str[i + 1])))
                            str = str.Remove(i, 1);
                        else
                            i++;
                    }
                    else if ((in_backtick || in_triple_backtick) && str.IndexOf("\\`", i) == i)
                    {
                        in_escape = true;
                        str = str.Remove(i, 1);
                    }
                }

                if (str[i] == '`' && !in_escape)
                {
                    if (in_triple_backtick && str.IndexOf("```", i) == i)
                    {
                        in_triple_backtick = false;
                        i += 2;
                    }
                    else if (!in_backtick && str.IndexOf("```", i) == i)
                    {
                        in_triple_backtick = true;
                        i += 2;
                    }

                    if (in_backtick && str.IndexOf("```", i) != i)
                        in_backtick = false;
                    else if (!in_triple_backtick && str.IndexOf("```", i) == i)
                        in_backtick = true;
                }

                if (str[i] == '"' && !in_escape && !in_backtick && !in_triple_backtick)
                {
                    str = str.Remove(i, 1);
                    i--;

                    if (!in_quote)
                        in_quote = true;
                    else
                        in_quote = false;
                }

                if (in_escape)
                    in_escape = false;

                if (ep != -1)
                {
                    remainder = str.Substring(ep);
                    return str.Substring(0, ep);
                }
            }

            remainder = null;
            return str;
        }

        internal static object[] BindArguments(CommandContext ctx, bool ignore_surplus)
        {
            // достаем команду из контекста
            Command cmd = ctx.Command;

            // создаем массив для конечных аргументов, размером соответствующий их кол-ву
            object[] args = new object[cmd.Arguments.Count];

            // получаем необработанную строку из текущего контекста
            string argstr = ctx.RawArgumentString;
            // строка для необработанных аргументов в течении иттерации
            string argrmd = "";
            // строка для обработанного аргументв в течении иттерации
            string argv = "";

            // исходя из кол-ва аргументов ...
            for (int i = 0; i < ctx.Command.Arguments.Count; i++)
            {
                // каждый аргумент в команде
                CommandArgument arg = ctx.Command.Arguments[i];
                // если перехватывает все остальные аргументы
                if (arg.IsCatchAll)
                {
                    // если аргумент является массивом
                    if (arg.IsArray)
                    {
                        // создаем лист для обработаных аргументов
                        List<object> lst = new List<object>();
                        while (true)
                        {
                            // достаем из необработанной строки аргументов каждый следующий аргумент
                            argv = ExtractNextArgument(argstr, out argrmd);
                            // до тех пор пока не дойдем до последнего
                            if (argv == null)
                                break;

                            // каждый раз передавая предыдущему оператору необработаную строку аргументов, исключаяя уже обработаный
                            argstr = argrmd;
                            // и добавляем в лист обработаный при помощи конвертера аргумент
                            lst.Add(argv.ConvertArgument(ctx, arg.Type));
                        }
                        // инициализируем массив аргументов с типом текущего аргумента и количеством всех аргументов
                        Array arr = Array.CreateInstance(ProviderConverters.GetConverter(arg.Type).TypeArgument, lst.Count);
                        // на кой-то хуй приводим лист сконвертированых аргументов к IList
                        // и пихаем их все в инициализированый массив
                        (lst as System.Collections.IList).CopyTo(arr, 0);
                        // кладем в результат всю эту срань аргументов в виде массива
                        args[i] = arr;

                        // чистим иттерационную строку, что бы при проверке знать что мы закончили с этим блядским аргументом
                        argstr = string.Empty;
                        // закончили с конвертацией аргументов
                        break;
                    }
                    // ну а если аргумент не является массивом, то ...
                    else
                    {
                        // аргумент не массив но значения нет ...
                        if (argstr == null)
                        {
                            // значит ставим стандартное
                            args[i] = arg.DefaultValue;
                            // и досвидули
                            break;
                        }

                        // ну а если значение есть ...
                        int j = 0;
                        // находим начало следующего аргумента
                        for (; j < argstr.Length; j++)
                            if (!char.IsWhiteSpace(argstr[j]))
                                break;
                        // и если он не в нулевой позиции
                        if (j > 0)
                            // подрезаем начало общей строки всех аргументов
                            argstr = argstr.Substring(j);

                        // сохраняем его в строку обработанного аргумента
                        argv = argstr;
                        // ну и конвертируем этот аргумент, с присвоением его к результатам
                        args[i] = argv.ConvertArgument(ctx, arg.Type);

                        // чистим иттерационную строку, что бы при проверке знать что мы закончили с этим блядским аргументом
                        argstr = string.Empty;
                        // закончили с конвертацией аргументов
                        break;
                    }
                }
                // если не перехватывает остальные аргументы
                else
                {
                    // то аккуратно каждый аргумент конвертируем
                    // и кладем во временную строку
                    argv = ExtractNextArgument(argstr, out argrmd);
                }

                // проверяем (на ноль) && (на обязательность) && (и если он не должен всех перехватывать)
                if (argv == null && !arg.IsOptional && !arg.IsCatchAll)
                    // косяк
                    throw new ArgumentException("Недостаточно аргументов для команды.");

                // то есть пустое значение может быть только у:
                // необязательного аргумента
                // и аргумента который не перехватывает остальные
                else if (argv == null)
                {
                    // кладем стандартное значение
                    args[i] = arg.DefaultValue;
                }
                else
                {
                    // ну а иначе конвертируем пользовательское и кладем в результат
                    args[i] = argv.ConvertArgument(ctx, arg.Type);
                }
                // кладем остаток строки во временную переменную
                argstr = argrmd;
            }
            // и смотрим если еще что то осталось, а у нас нет экстра аргументов то...
            if (!ignore_surplus && !string.IsNullOrWhiteSpace(argstr))
                // чет дохуя осталось
                throw new ArgumentException("Слишком много аргументов было предоставлено этой команде.");

            // возвращаем результат 
            return args;
        }

        #endregion
    }
}