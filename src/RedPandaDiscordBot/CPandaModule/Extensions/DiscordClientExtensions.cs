﻿using DSharpPlus;
using System;

namespace RedPandaDiscordBot.CPandaModule.Extensions
{
    static class DiscordClientExtensions
    {
        public static CommandPandaModule UseCommandsPanda(this DiscordClient client, CommandsPandaConfiguration config)
        {
            if (client.GetModule<CommandPandaModule>() != null)
                throw new InvalidOperationException("CommandPandaModule уже установлен в DiscordClient.");

            CommandPandaModule cPandaModule = new CommandPandaModule(config);
            client.AddModule(cPandaModule);
            return cPandaModule;
        }

        /// <summary>
        /// Возращает установленный CommandPanda модуль в этом клиенте.
        /// </summary>
        /// <param name="client">Клиент в котором установлен модуль</param>
        /// <returns>Модуль Panda, или Null если не установлен.</returns>
        public static CommandPandaModule GetCommandsPanda(this DiscordClient client)
        {
            return client.GetModule<CommandPandaModule>();
        }
    }
}