﻿using Newtonsoft.Json.Linq;
using System.IO;

namespace RedPandaDiscordBot.CPandaModule.Extensions
{
    public class JConfig : JObject
    {
        public JConfig() : base() { }

        public new string this[string key] => TryGetValue(key, out JToken res)?  res.ToString() : "";

        public void AddJsonFile(string path, bool rewrite = true)
        {
            string json = "";
            using (StreamReader sr = new StreamReader(path))
                json = sr.ReadToEnd();

            Merge(Parse(json), new JsonMergeSettings
            {
                // union array values together to avoid duplicates
                MergeArrayHandling = MergeArrayHandling.Union
            });
        }

        public string GetJson()
        {
            return Root.ToString();
        }
    }
}