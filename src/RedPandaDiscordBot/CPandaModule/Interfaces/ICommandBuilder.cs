﻿using DSharpPlus;
using RedPandaDiscordBot.CBuilder.EventArgs;
using RedPandaDiscordBot.CPandaModule.Cmd;

namespace RedPandaDiscordBot.CPandaModule.Interfaces
{
    public interface ICommandBuilder
    {
        /// <summary>
        /// Ошибки билдера
        /// </summary>
        event AsyncEventHandler<BuilderEventArgs> ErrorEvent;

        /// <summary>
        /// Версия билдера
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Собирает команды из Json строки
        /// </summary>
        /// <param name="json">Строка Json содержащая сериализованные команды</param>
        /// <returns>Список собранных команд из строки Json</returns>
        Command BuildFromJson(string json);
    }
}