﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.CPandaModule.TypeConverters
{
    /// <summary>
    /// Базовый класс для всех конвертеров
    /// </summary>
    public abstract class BaseTypeConverter
    {
        /// <summary>
        /// Тип аргументов для конвертации
        /// </summary>
        public Type TypeArgument { get; set; }

        /// <summary>
        /// Имя типа
        /// </summary>
        public string NameType { get; protected set; }

        /// <summary>
        /// Локализированое имя типа
        /// </summary>
        public string NameLocal { get; protected set; }

        /// <summary>
        /// Локализированое описание типа
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// Конвертирует аргумент в тип со значением
        /// </summary>
        /// <param name="argument">Строка аргумента</param>
        /// <param name="convertArg">Сконвертированый тип со значением</param>
        /// <returns>Возращаеет обьект </returns>
        public abstract bool TryConvert(string argument, CommandContext ctx, out object convertArg);
    }
}