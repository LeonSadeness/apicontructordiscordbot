﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.CPandaModule.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace RedPandaDiscordBot.CPandaModule.TypeConverters.Converters
{
    public class DiscordUserConverter : BaseTypeConverter
    {
        private static Regex _userRegex { get; set; }

        static DiscordUserConverter()
        {
            _userRegex = new Regex(@"<@\!?(\d+?)>", RegexOptions.ECMAScript);
        }

        public DiscordUserConverter()
        {
            NameType = "user";
            TypeArgument = typeof(DiscordUser);
            NameLocal = "Пользователь";
            Description = "Пользователь Discord (ник и дескриминатор)";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong uid))
            {
                convertArg = ctx.Client.GetUserAsync(uid).GetAwaiter().GetResult();
                return true;
            }

            Match m = _userRegex.Match(argument);
            if (m.Success && ulong.TryParse(m.Groups[1].Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out uid))
            {
                convertArg = ctx.Client.GetUserAsync(uid).GetAwaiter().GetResult();
                return true;
            }

            argument = argument.ToLowerInvariant();
            int di = argument.IndexOf('#');
            string un = di != -1 ? argument.Substring(0, di) : argument;
            string dv = di != -1 ? argument.Substring(di + 1) : null;

            IEnumerable<DiscordMember> us = ctx.Client.Guilds
                .SelectMany(xkvp => xkvp.Value.Members)
                .Where(xm => xm.Username.ToLowerInvariant() == un && ((dv != null && xm.Discriminator == dv) || dv == null));

            convertArg = us.FirstOrDefault();
            return convertArg != null;
        }
    }

    public class DiscordMemberConverter : BaseTypeConverter
    {
        private static Regex UserRegex { get; set; }

        static DiscordMemberConverter()
        {
            UserRegex = new Regex(@"<@\!?(\d+?)>", RegexOptions.ECMAScript);
        }

        public DiscordMemberConverter()
        {
            NameType = "member";
            TypeArgument = typeof(DiscordMember);
            NameLocal = "Пользователь";
            Description = "Пользователь Discord текущего сервера (ник и дескриминатор)";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong uid))
            {
                convertArg = ctx.Guild.GetMemberAsync(uid).GetAwaiter().GetResult();
                return true;
            }

            Match m = UserRegex.Match(argument);
            if (m.Success && ulong.TryParse(m.Groups[1].Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out uid))
            {
                convertArg = ctx.Guild.GetMemberAsync(uid).GetAwaiter().GetResult();
                return true;
            }

            argument = argument.ToLowerInvariant();
            int di = argument.IndexOf('#');
            string un = di != -1 ? argument.Substring(0, di) : argument;
            string dv = di != -1 ? argument.Substring(di + 1) : null;

            IEnumerable<DiscordMember> us = ctx.Guild.Members
                .Where(xm => (xm.Username.ToLowerInvariant() == un && ((dv != null && xm.Discriminator == dv) || dv == null)) || xm.Nickname?.ToLowerInvariant() == argument);

            convertArg = us.FirstOrDefault();
            return convertArg != null;
        }
    }

    public class DiscordRoleConverter : BaseTypeConverter
    {
        private static Regex RoleRegex { get; set; }

        static DiscordRoleConverter()
        {
            RoleRegex = new Regex(@"<@&(\d+?)>", RegexOptions.ECMAScript);
        }

        public DiscordRoleConverter()
        {
            NameType = "role";
            TypeArgument = typeof(DiscordRole);
            NameLocal = "Роль";
            Description = "Название роли на сервере(@myrole)";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong rid))
            {
                convertArg = ctx.Guild.Roles.FirstOrDefault(xr => xr.Id == rid);
                return true;
            }

            Match m = RoleRegex.Match(argument);
            if (m.Success && ulong.TryParse(m.Groups[1].Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out rid))
            {
                convertArg = ctx.Guild.Roles.FirstOrDefault(xr => xr.Id == rid);
                return true;
            }

            var rl = ctx.Guild.Roles.FirstOrDefault(xr => xr.Name == argument);
            convertArg = rl;
            return convertArg != null;
        }
    }

    public class DiscordChannelConverter : BaseTypeConverter
    {
        private static Regex _channelRegex { get; set; }

        static DiscordChannelConverter()
        {
            _channelRegex = new Regex(@"<#(\d+)>", RegexOptions.ECMAScript);
        }

        public DiscordChannelConverter()
        {
            NameType = "channel";
            TypeArgument = typeof(DiscordChannel);
            NameLocal = "Канал";
            Description = "Имя канала сервера";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong cid))
            {
                convertArg = ctx.Guild.Channels.FirstOrDefault(xc => xc.Id == cid);
                return true;
            }

            Match m = _channelRegex.Match(argument);
            if (m.Success && ulong.TryParse(m.Groups[1].Value, NumberStyles.Integer, CultureInfo.InvariantCulture, out cid))
            {
                convertArg = ctx.Guild.Channels.FirstOrDefault(xc => xc.Id == cid);
                return true;
            }

            DiscordChannel chn = ctx.Guild.Channels.FirstOrDefault(xc => xc.Name == argument);
            convertArg = chn;
            return convertArg != null;
        }
    }

    public class DiscordGuildConverter : BaseTypeConverter
    {
        public DiscordGuildConverter()
        {
            NameType = "guild";
            TypeArgument = typeof(DiscordGuild);
            NameLocal = "Сервер";
            Description = "Имя Discord  сервера (гильдия)";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong gid))
            {
                bool ret = ctx.Client.Guilds.TryGetValue(gid, out DiscordGuild result);
                convertArg = result;
                return ret;
            }

            if (ctx.Client.Guilds.Any(xg => xg.Value.Name == argument))
            {
                convertArg = ctx.Client.Guilds.First(xg => xg.Value.Name == argument).Value;
                return true;
            }

            convertArg = null;
            return false;
        }
    }

    public class DiscordMessageConverter : BaseTypeConverter
    {
        public DiscordMessageConverter()
        {
            NameType = "message";
            TypeArgument = typeof(DiscordMessage);
            NameLocal = "Сообщение";
            Description = "Сообщение чата Discord";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong mid))
            {
                convertArg = ctx.Channel.GetMessageAsync(mid).GetAwaiter().GetResult();
                return true;
            }

            convertArg = null;
            return false;
        }
    }

    public class DiscordEmojiConverter : BaseTypeConverter
    {
        private static Regex _emoteRegex { get; set; }

        static DiscordEmojiConverter()
        {
            _emoteRegex = new Regex(@"<:([a-zA-Z0-9_]+?):(\d+?)>", RegexOptions.ECMAScript);
        }

        public DiscordEmojiConverter()
        {
            NameType = "emoji";
            TypeArgument = typeof(DiscordEmoji);
            NameLocal = "Смайл";
            Description = "Строковый или символьный идентификатор смайлика";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            if (DiscordEmojiList.GetUnicodeEmojiList().Contains(argument))
            {
                convertArg = DiscordEmoji.FromUnicode(ctx.Client, argument);
                return true;
            }

            Match m = _emoteRegex.Match(argument);
            if (m.Success)
            {
                ulong id = ulong.Parse(m.Groups[2].Value, CultureInfo.InvariantCulture);
                convertArg = DiscordEmoji.FromGuildEmote(ctx.Client, id);
                return true;
            }

            convertArg = null;
            return false;
        }
    }
}