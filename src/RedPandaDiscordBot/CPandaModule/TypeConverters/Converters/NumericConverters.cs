﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using System.Globalization;

namespace RedPandaDiscordBot.CPandaModule.TypeConverters.Converters
{
    public class BoolConverter : BaseTypeConverter
    {
        public BoolConverter()
        {
            NameType = "boolean";
            TypeArgument = typeof(bool);
            NameLocal = "Булево";
            Description = "Булево значение содержит Правду или Ложь";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = bool.TryParse(argument, out bool result);
            convertArg = result;
            return ret;
        }
    }

    public class Int8Converter : BaseTypeConverter
    {
        public Int8Converter()
        {
            NameType = "signed byte";
            TypeArgument = typeof(sbyte);
            NameLocal = "Целое число";
            Description = "8-разрядное целое число со знаком";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = sbyte.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out sbyte result);
            convertArg = result;
            return ret;
        }
    }

    public class Uint8Converter : BaseTypeConverter
    {
        public Uint8Converter()
        {
            NameType = "byte";
            TypeArgument = typeof(byte);
            NameLocal = "Целое число";
            Description = "8-разрядное целое число без знака";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = byte.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out byte result);
            convertArg = result;
            return ret;
        }
    }

    public class Int16Converter : BaseTypeConverter
    {
        public Int16Converter()
        {
            NameType = "short";
            TypeArgument = typeof(short);
            NameLocal = "Целое число";
            Description = "16-разрядное целое число со знаком";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = short.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out short result);
            convertArg = result;
            return ret;
        }
    }

    public class Uint16Converter : BaseTypeConverter
    {
        public Uint16Converter()
        {
            NameType = "unsigned short";
            TypeArgument = typeof(ushort);
            NameLocal = "Целое число";
            Description = "16-разрядное целое число без знака";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = ushort.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ushort result);
            convertArg = result;
            return ret;
        }
    }

    public class Int32Converter : BaseTypeConverter
    {
        public Int32Converter()
        {
            NameType = "int";
            TypeArgument = typeof(int);
            NameLocal = "Целое число";
            Description = "32-разрядное целое число со знаком";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = int.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out int result);
            convertArg = result;
            return ret;
        }
    }

    public class Uint32Converter : BaseTypeConverter
    {
        public Uint32Converter()
        {
            NameType = "unsigned int";
            TypeArgument = typeof(uint);
            NameLocal = "Целое число";
            Description = "32-разрядное целое число без знака";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = uint.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out uint result);
            convertArg = result;
            return ret;
        }
    }

    public class Int64Converter : BaseTypeConverter
    {
        public Int64Converter()
        {
            NameType = "long";
            TypeArgument = typeof(long);
            NameLocal = "Целое число";
            Description = "64-разрядное целое число со знаком";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = long.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out long result);
            convertArg = result;
            return ret;
        }
    }

    public class Uint64Converter : BaseTypeConverter
    {
        public Uint64Converter()
        {
            NameType = "unsigned long";
            TypeArgument = typeof(ulong);
            NameLocal = "Целое число";
            Description = "64-разрядное целое число без знака";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = ulong.TryParse(argument, NumberStyles.Integer, CultureInfo.InvariantCulture, out ulong result);
            convertArg = result;
            return ret;
        }
    }

    public class Float32Converter : BaseTypeConverter
    {
        public Float32Converter()
        {
            NameType = "float";
            TypeArgument = typeof(float);
            NameLocal = "Дробное число";
            Description = "Число с плавающей запятой";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = float.TryParse(argument, NumberStyles.Number, CultureInfo.InvariantCulture, out float result);
            convertArg = result;
            return ret;
        }
    }

    public class Float64Converter : BaseTypeConverter
    {
        public Float64Converter()
        {
            NameType = "double";
            TypeArgument = typeof(double);
            NameLocal = "Дробное число";
            Description = "Число с плавающей запятой";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = double.TryParse(argument, NumberStyles.Number, CultureInfo.InvariantCulture, out double result);
            convertArg = result;
            return ret;
        }
    }

    public class Float128Converter : BaseTypeConverter
    {
        public Float128Converter()
        {
            NameType = "decimal";
            TypeArgument = typeof(decimal);
            NameLocal = "Дробное число";
            Description = "Число с плавающей запятой";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = decimal.TryParse(argument, NumberStyles.Number, CultureInfo.InvariantCulture, out decimal result);
            convertArg = result;
            return ret;
        }
    }
}