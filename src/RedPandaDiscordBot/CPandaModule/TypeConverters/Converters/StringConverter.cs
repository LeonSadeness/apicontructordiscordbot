﻿using RedPandaDiscordBot.CPandaModule.EventArgs;

namespace RedPandaDiscordBot.CPandaModule.TypeConverters.Converters
{
    public class StringConverter : BaseTypeConverter
    {
        public StringConverter()
        {
            NameType = "string";
            TypeArgument = typeof(string);
            NameLocal = "Строка";
            Description = "Строковое значение, содержащее символы";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            convertArg = argument;
            return true;
        }
    }
}