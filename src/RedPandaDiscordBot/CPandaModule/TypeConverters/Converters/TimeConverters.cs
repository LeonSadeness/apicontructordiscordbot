﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace RedPandaDiscordBot.CPandaModule.TypeConverters.Converters
{
    public class DateTimeConverter : BaseTypeConverter
    {
        public DateTimeConverter()
        {
            NameType = "date time";
            TypeArgument = typeof(DateTime);
            NameLocal = "Дата";
            Description = "Определенная дата и время в строковом представлении";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = DateTime.TryParse(argument, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result);
            convertArg = result;
            return ret;
        }
    }

    public class DateTimeOffsetConverter : BaseTypeConverter
    {
        public DateTimeOffsetConverter()
        {
            NameType = "date time offset";
            TypeArgument = typeof(DateTimeOffset);
            NameLocal = "Смещенная дата";
            Description = "Определенная дата и время относительно UTC в строковом представлении";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            bool ret = DateTimeOffset.TryParse(argument, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTimeOffset result);
            convertArg = result;
            return ret;
        }
    }

    public class TimeSpanConverter : BaseTypeConverter
    {
        private static Regex _timeSpanRegex { get; set; }

        static TimeSpanConverter()
        {
            _timeSpanRegex = new Regex(@"^(?<days>\d+d)?(?<hours>\d{1,2}h)?(?<minutes>\d{1,2}m)?(?<seconds>\d{1,2}s)?$", RegexOptions.ECMAScript);
        }

        public TimeSpanConverter()
        {
            NameType = "time span";
            TypeArgument = typeof(TimeSpan);
            NameLocal = "Интервал";
            Description = "Промежуток времени в строковом представлении";
        }

        public override bool TryConvert(string argument, CommandContext ctx, out object convertArg)
        {
            convertArg = TimeSpan.Zero;
            if (argument == "0")
                return true;

            if (TimeSpan.TryParse(argument, CultureInfo.InvariantCulture, out TimeSpan result))
            {
                convertArg = result;
                return true;
            }

            string[] gps = new string[] { "days", "hours", "minutes", "seconds" };
            Match mtc = _timeSpanRegex.Match(argument);
            if (!mtc.Success)
                return false;

            int d = 0;
            int h = 0;
            int m = 0;
            int s = 0;
            foreach (string gp in gps)
            {
                string gpc = mtc.Groups[gp].Value;
                if (string.IsNullOrWhiteSpace(gpc))
                    continue;

                char gpt = gpc[gpc.Length - 1];
                int.TryParse(gpc.Substring(0, gpc.Length - 1), NumberStyles.Integer, CultureInfo.InvariantCulture, out int val);
                switch (gpt)
                {
                    case 'd':
                        d = val;
                        break;

                    case 'h':
                        h = val;
                        break;

                    case 'm':
                        m = val;
                        break;

                    case 's':
                        s = val;
                        break;
                }
            }
            convertArg = new TimeSpan(d, h, m, s);
            return true;
        }
    }
}