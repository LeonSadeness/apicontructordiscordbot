﻿using System;
using System.Linq;
using System.Collections.Generic;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.CPandaModule.TypeConverters.Converters;
using RedPandaDiscordBot.Services;
using Newtonsoft.Json.Linq;

namespace RedPandaDiscordBot.CPandaModule.TypeConverters
{
    public static class ProviderConverters
    {
        private static IList<BaseTypeConverter> _listConverters;
        public static IList<BaseTypeConverter> ListConverters { get => _listConverters;}

        static ProviderConverters()
        {
            _listConverters = new List<BaseTypeConverter>();

            #region Add Converters

            // строковые
            _listConverters.Add(new StringConverter());
            // численые
            _listConverters.Add(new BoolConverter());
            _listConverters.Add(new Int8Converter());
            _listConverters.Add(new Uint8Converter());
            _listConverters.Add(new Int16Converter());
            _listConverters.Add(new Uint16Converter());
            _listConverters.Add(new Int32Converter());
            _listConverters.Add(new Uint32Converter());
            _listConverters.Add(new Int64Converter());
            _listConverters.Add(new Uint64Converter());
            _listConverters.Add(new Float32Converter());
            _listConverters.Add(new Float64Converter());
            _listConverters.Add(new Float128Converter());
            // модельные
            _listConverters.Add(new DiscordUserConverter());
            _listConverters.Add(new DiscordMemberConverter());
            _listConverters.Add(new DiscordRoleConverter());
            _listConverters.Add(new DiscordChannelConverter());
            _listConverters.Add(new DiscordGuildConverter());
            _listConverters.Add(new DiscordMessageConverter());
            _listConverters.Add(new DiscordEmojiConverter());
            // хронологичные
            _listConverters.Add(new DateTimeConverter());
            _listConverters.Add(new DateTimeOffsetConverter());
            _listConverters.Add(new TimeSpanConverter());

            #endregion
        }

        /// <summary>
        /// Возвращает имя типа в соответствии с именами типов в конвертерах 
        /// </summary>
        /// <param name="type">тип для получения имени</param>
        /// <returns>строковое представление имени типа</returns>
        public static string GetNameFromType(Type type)
        {
            string result = GetConverter(type)?.NameType;
            if(result == null)
            {
                result = type.Name;
            }
            return result;
        }

        /// <summary>
        /// Возращает типизированый конвертер значений
        /// </summary>
        /// <param name="type">Тип переменной, в которую нужно сконвертировать</param>
        /// <returns>Возращает типизированый конвертер унаследованный от BaseTypeConverter</returns>
        public static BaseTypeConverter GetConverter(Type type)
        {
            return ListConverters.FirstOrDefault(cnv => cnv.TypeArgument == type);
        }

        /// <summary>
        /// Возращает типизированый конвертер значений
        /// </summary>
        /// <param name="type">Тип переменной, в которую нужно сконвертировать в строковом представлении</param>
        /// <returns>Возращает типизированый конвертер унаследованный от BaseTypeConverter</returns>
        public static BaseTypeConverter GetConverter(string type)
        {
            return ListConverters.FirstOrDefault(cnv => cnv.NameType == type.ToLower());
        }

        /// <summary>
        /// Преобразует строку в указанный тип.
        /// </summary>
        /// <param name="value">Значение для конвертирования</param>
        /// <param name="ctx">Контекст для конвертации</param>
        /// <param name="type">Тип для конвертации</param>
        /// <returns>Возращает сконвертированный обьект</returns>
        public static object ConvertArgument(this string value, CommandContext ctx, Type type)
        {
            BaseTypeConverter converter = GetConverter(type);
            if(!converter.TryConvert(value, ctx, out object converted))
                throw new ArgumentException("Не удалось преобразовать указанное значение в данный тип.", nameof(value));
            return converted;
        }

        /// <summary>
        /// Преобразует строку в указанный тип.
        /// </summary>
        /// <param name="value">Значение для конвертирования</param>
        /// <param name="ctx">Контекст для конвертации</param>
        /// <param name="type">Тип для конвертации в строковом представлении</param>
        /// <returns>Возращает сконвертированный обьект</returns>
        public static object ConvertArgument(this string value, CommandContext ctx, string type)
        {
            BaseTypeConverter converter = GetConverter(type);
            if (!converter.TryConvert(value, ctx, out object converted))
                throw new ArgumentException("Не удалось преобразовать указанное значение в данный тип.", nameof(value));
            return converted;
        }

        /// <summary>
        /// Возвращает описание всех типов в Json строке.
        /// </summary>
        /// <returns></returns>
        public static string GetDescriptionTypeJson()
        {
            if (Settings.Env.ToLower().StartsWith("dev"))
            {
                string json = "";
                if (ListConverters != null && ListConverters.Count > 0)
                {
                    JArray jArray = new JArray(
                        from type in ListConverters
                        select new JObject(
                        new JProperty("NameType", type.NameType),
                        new JProperty("NameLocal", type.NameLocal),
                        new JProperty("Description", type.Description)
                    ));
                    json = jArray.ToString(Newtonsoft.Json.Formatting.Indented);
                }
                return json;
            }
            else
            {
                return $"Функции разработчика запрещены в текущем окружении - \"{Settings.Env}\"";
            }
        }
    }
}