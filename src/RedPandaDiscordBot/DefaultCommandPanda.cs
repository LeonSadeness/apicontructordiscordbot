﻿using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.Cmd.Blocks;
using System.Collections.Generic;

namespace RedPandaDiscordBot
{
    /// <summary>
    /// Стандартные команды для бота
    /// </summary>
    internal class DefaultCommandPanda
    {
        /// <summary>
        /// Лист стандартных комманд.
        /// </summary>
        public static List<Command> Commands => _commands ?? (_commands = InitCommands());
        private static List<Command> _commands;

        /// <summary>
        /// Инициализация команд
        /// </summary>
        /// <returns>Лист инициализированных команд</returns>
        static List<Command> InitCommands()
        {
            Command cmd_Build = new Command
            {
                Version = "1.0",
                Name = "build",
                Aliases = new List<string>() { "bld" },
                IsHidden = true,
                Description = "Собирает команду из Json",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        DefaultValue = "",
                        Name = "Json",
                        Description = "Команда в формате Json строки",
                        IsCatchAll = true,
                        IsOptional = false,
                        Type = "string",
                    }
                },
                Constants = new BlockDefaultArgument[]
                {
                    new BlockDefaultArgument
                    {
                        TypeValue = "boolean",
                        Value = "true"
                    },
                    new BlockDefaultArgument
                    {
                        TypeValue = "string",
                        Value = "8 268435456"
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            FunctionName = "FuncUserPermissionAny",
                            BlockTypeF = BlockType.OPERATION,
                            ArgsAddresses = new string[] { "1 1" },
                        },
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.IF,
                            IfCount = 10,
                            ArgsAddresses = new string[] { "2 0.0 0" },
                            InternalBlocks = new CommandBlock[]
                            {
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {"0 0" },
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncBuildCommand",
                                },
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {"2 0.1.0 0", "1 0" },
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncRegisteredCmd",
                                },
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {"2 0.1.0 1", "2 0.1.1 1", "1 0"},
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncStringContact",
                                },
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {null, "2 0.1.2 0", null },
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncSendChannelMessage",
                                }
                            }
                        }
                    }
                }
            };

            Command cmd_DelCmd = new Command
            {
                Version = "1.0",
                Name = "delcmd",
                Aliases = new List<string>() { "dcmd" },
                IsHidden = true,
                Description = "Удаляет команду по имени",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        DefaultValue = "",
                        Name = "Имя",
                        Description = "Имя, или имена команд через пробел",
                        Type = "String",
                        IsArray = true,
                        IsCatchAll = true,
                        IsOptional = false,
                    }
                },
                Constants = new BlockDefaultArgument[]
                {
                    new BlockDefaultArgument
                    {
                        TypeValue = "boolean",
                        Value = "false"
                    },
                    new BlockDefaultArgument
                    {
                        TypeValue = "string",
                        Value = "8 268435456"
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock{
                            FunctionName = "FuncUserPermissionAny",
                            BlockTypeF = BlockType.OPERATION,
                            ArgsAddresses = new string[] { "1 1" }
                        },
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.IF,
                            IfCount = 3,
                            ArgsAddresses = new string[] { "2 0.0 0" },
                            InternalBlocks = new CommandBlock[]
                            {
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {"0 0" },
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncFindCommand",
                                },
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {"2 0.1.0 0", "1 0" },
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncRegisteredCmd",
                                },
                                new CommandBlock
                                {
                                    ArgsAddresses =  new string[] {null, "2 0.1.1 1", null },
                                    BlockTypeF = BlockType.OPERATION,
                                    FunctionName = "FuncSendChannelMessage",
                                }
                            }
                        }
                    }
                }
            };

            Command cmd_WhoIs = new Command
            {
                Version = "1.0",
                Name = "is",
                Aliases = new List<string>() { "who" },
                IsHidden = false,
                Description = "Определятор 2000",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        Name = "Пользователь",
                        Description = "Идентификатор или имя пользователя",
                        Type = "user",
                        IsCatchAll = true
                    }
                },
                Constants = new BlockDefaultArgument[]
                {
                    new BlockDefaultArgument
                    {
                        Value = " :robot:",
                        TypeValue = "string"
                    },
                    new BlockDefaultArgument
                    {
                        Value = " :bust_in_silhouette:",
                        TypeValue = "string"
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            ArgsAddresses = new string[] { "0 0" },
                            BlockTypeF = BlockType.OPERATION,
                            FunctionName = "FuncUserIsBot",
                        },
                        new CommandBlock
                        {
                            ArgsAddresses = new string[] { "2 0.0 0" },
                            BlockTypeF = BlockType.IF,
                            IfCount = 1,
                            FunctionName = "FuncUserIsBot",
                            InternalBlocks = new CommandBlock[]
                            {
                                new CommandBlock
                                {
                                    ArgsAddresses = new string[] { null, "1 0", null},
                                    BlockTypeF = BlockType.OPERATION,
                                    IfCount = 0,
                                    FunctionName = "FuncSendChannelMessage",
                                    InternalBlocks = null,
                                },
                                new CommandBlock
                                {
                                    ArgsAddresses = new string[] { null, "1 1", null},
                                    BlockTypeF = BlockType.OPERATION,
                                    IfCount = 0,
                                    FunctionName = "FuncSendChannelMessage",
                                    InternalBlocks = null,
                                }
                            }
                        }
                    }
                }
            };

            Command cmd_Config = new Command
            {
                Version = "1.0",
                Name = "config",
                Aliases = new List<string>() { "conf", "cfg" },
                Description = "Возращает конфигурацию, или его значение по ключу",
                IsHidden = true,
                Constants = new BlockDefaultArgument[]
                {
                    new BlockDefaultArgument
                    {
                        TypeValue = "int",
                        Value = "2000"
                    },
                    new BlockDefaultArgument
                    {
                        TypeValue = "string",
                        Value = "8 268435456"
                    }
                },
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        Name = "Ключ",
                        Description = "Ключ параметра конфигурации",
                        Type = "string",
                        IsOptional = true,
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            FunctionName = "FuncUserPermissionAny",
                            BlockTypeF = BlockType.OPERATION,
                            ArgsAddresses = new string[] { "1 1" }
                        },
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.IF,
                            IfCount = 3,
                            ArgsAddresses = new string[] { "2 0.0 0" },
                            InternalBlocks = new CommandBlock[]
                            {
                                new CommandBlock
                                {
                                    FunctionName = "FuncConfig",
                                    BlockTypeF = BlockType.OPERATION,
                                    ArgsAddresses =  new string[] {"0 0"}
                                },
                                new CommandBlock
                                {
                                    FunctionName = "FuncStrLength",
                                    BlockTypeF = BlockType.OPERATION,
                                    ArgsAddresses =  new string[] { "2 0.1.0 0" }
                                },
                                new CommandBlock
                                {
                                    FunctionName = "FuncLarger",
                                    BlockTypeF = BlockType.OPERATION,
                                    ArgsAddresses = new string[] { "2 0.1.1 0", "1 0" },
                                },
                                new CommandBlock
                                {
                                    BlockTypeF = BlockType.IF,
                                    ArgsAddresses = new string[] { "2 0.1.2 0" },
                                    IfCount = 1,
                                    InternalBlocks = new CommandBlock[]
                                    {
                                        new CommandBlock
                                        {
                                            FunctionName = "FuncSendChannelMessage",
                                            BlockTypeF = BlockType.OPERATION,
                                            ArgsAddresses =  new string[] { null, null, "2 0.0 1" }
                                        },
                                        new CommandBlock
                                        {
                                            FunctionName = "FuncSendChannelMessage",
                                            BlockTypeF = BlockType.OPERATION,
                                            ArgsAddresses =  new string[] { null, "2 0.0 0", null }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            Command cmd_DelMsg = new Command
            {
                Version = "1.0",
                Name = "delete",
                Aliases = new List<string>() { "del" },
                IsHidden = true,
                Description = "Удаляет указанное кол-во сообщений в чате",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        Name = "Число",
                        Description = "Количество удаляемых сообщений из чата",
                        Type = "int",
                        IsOptional = true,
                    }
                },
                Constants = new BlockDefaultArgument[]
                {
                    new BlockDefaultArgument
                    {
                        TypeValue = "string",
                        Value = "8 268435456"
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            FunctionName = "FuncUserPermissionAny",
                            BlockTypeF = BlockType.OPERATION,
                            ArgsAddresses = new string[] { "1 0" },
                        },
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.IF,
                            IfCount = 3,
                            ArgsAddresses = new string[] { "2 0.0 0" },
                            InternalBlocks = new CommandBlock[]
                            {
                                new CommandBlock
                                {
                                    FunctionName = "FuncDeleteMessages",
                                    BlockTypeF = BlockType.OPERATION,
                                    ArgsAddresses = new string[] { null, "0 0" }
                                }
                            }
                        }
                    }
                }
            };

            Command cmd_Joke = new Command
            {
                Version = "1.0",
                Name = "Joke",
                Aliases = new List<string>() { "шутка" },
                IsHidden = false,
                Description = "Выдает шутку в чат в нужном кол-ве. Max=3",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        Name = "Число",
                        Description = "Кол-во шуток. До 3-х",
                        Type = "int",
                        IsOptional = true,
                        DefaultValue = "1"
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    FunctionName = "",
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.OPERATION,
                            FunctionName = "FuncJoke",
                            ArgsAddresses =  new string[] {"0 0"}
                        },
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.OPERATION,
                            FunctionName = "FuncSendChannelMessage",
                            ArgsAddresses = new string[] { null, "2 0.0 0", null }
                        }
                    },
                }
            };

            Command cmd_Translate = new Command
            {
                Name = "translate",
                Aliases = new List<string>() { "t" },
                IsHidden = false,
                Description = "Пeреводчик на русский язык",
                Arguments = new List<CommandArgument>
                {
                    new CommandArgument
                    {
                        DefaultValue = "",
                        Name = "Текст",
                        Description = "Текст для перевода на русский",
                        IsCatchAll = true,
                        IsOptional = false,
                        Type = "string"
                    }
                },
                Blocks = new CommandBlock
                {
                    BlockTypeF = BlockType.OPERATION,
                    FunctionName = "",
                    InternalBlocks = new CommandBlock[]
                    {
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.OPERATION,
                            FunctionName = "FuncTranslate",
                            ArgsAddresses =  new string[] {"0 0"}
                        },
                        new CommandBlock
                        {
                            BlockTypeF = BlockType.OPERATION,
                            FunctionName = "FuncSendChannelMessage",
                            ArgsAddresses = new string[] { null, "2 0.0 0", null }
                        }
                    },
                }
            };

            //Command cmd_08 = new Command
            //{
            //    Name = "remind",
            //    Aliases = new List<string>() { "напомни" },
            //    IsHidden = false,
            //    Description = "Напоминалка",
            //    Arguments = new List<CommandArgument>
            //    {
            //        new CommandArgument
            //        {
            //            DefaultValue = "",
            //            Description = "Получатель напоминания",
            //            IsCatchAll = false,
            //            IsOptional = false,
            //            Name = "адрес",
            //            Type = typeof(string),
            //            _is_array = false
            //        },
            //        new CommandArgument
            //        {
            //            DefaultValue = "в",
            //            Description = "предлог означающий промежуток (через) или точку (в)",
            //            IsCatchAll = false,
            //            IsOptional = false,
            //            Name = "предлог",
            //            Type = typeof(string),
            //            _is_array = false
            //        },
            //        new CommandArgument
            //        {
            //            DefaultValue = "",
            //            Description = "время или промежуток времени",
            //            IsCatchAll = false,
            //            IsOptional = false,
            //            Name = "время",
            //            Type = typeof(string),
            //            _is_array = false
            //        },
            //        new CommandArgument
            //        {
            //            DefaultValue = "",
            //            Description = "текст напоминания",
            //            IsCatchAll = true,
            //            IsOptional = false,
            //            Name = "текст",
            //            Type = typeof(string),
            //            _is_array = false
            //        },
            //},
            //    blocks = ReminderAsync
            //};

            // инициализация массива
            return new List<Command>
            {
                cmd_Build,
                cmd_DelCmd,
                cmd_WhoIs,
                cmd_Config,
                cmd_DelMsg,
                cmd_Joke,
                cmd_Translate
            };
        }

        //#region TestMethods

        //public async Task ReminderAsync(object[] args)
        //{
        //    if (args[0] is CommandContext ctx && args[1] is string name && args[2] is string pref && args[3] is string time && args[4] is string text)
        //    {
        //        string autor = "";
        //        await ctx.TriggerTypingAsync();

        //        if (name == "мне")
        //        {
        //            autor = " Ты просил";
        //            name = ctx.User.Mention;
        //        }
        //        else
        //        {
        //            var dm = name.ConvertArgument(ctx, typeof(DiscordMember));
        //            if (dm != null)
        //            {
        //                autor = " Меня попросил " + ctx.User.Username;
        //                name = ((DiscordMember)dm).Mention;
        //            }
        //        }
        //        if (autor == "")
        //        {
        //            await ctx.RespondAsync(ctx.User.Mention + "Не могу понять, кому сделать напоминание.");
        //            throw new Exception("Ошибка аргумента команды");
        //        }
        //        if (pref.Contains("в"))
        //        {
        //            if (time.Contains(':') && time.Length == 5 && int.TryParse(time.Substring(0, 2), out int hh) && int.TryParse(time.Substring(3, 2), out int mm))
        //            {
        //                if (hh >= DateTime.Now.Hour)
        //                {
        //                    if (hh < 24 && mm < 60)
        //                    {
        //                        hh -= DateTime.Now.Hour;
        //                        mm -= DateTime.Now.Minute;
        //                        await ctx.RespondAsync(ctx.User.Mention + " Ok");
        //                        var t = Task.Delay(DateTime.Now.AddHours(hh).AddMinutes(mm) - DateTime.Now);
        //                        t.Wait();
        //                        await ctx.RespondAsync(name + autor + " напомнить:\n**" + text + "**");
        //                    }
        //                    else await ctx.RespondAsync(ctx.User.Mention + "На этой планете максимальный формат времени 24-х часовой");
        //                }
        //                else await ctx.RespondAsync(ctx.User.Mention + " Я не умею телепортироваться в прошлое :laughing:");
        //            }
        //            else await ctx.RespondAsync(ctx.User.Mention + " Время указывается в 24-x часовом формате ЧЧ:ММ (пример 13:30)");
        //        }
        //        else if (pref.Contains("через"))
        //        {
        //            if (time.Contains(':') && time.Length == 5 && int.TryParse(time.Substring(0, 2), out int hh) && int.TryParse(time.Substring(3, 2), out int mm))
        //            {
        //                if (hh < 24 && mm < 60)
        //                {
        //                    await ctx.RespondAsync(ctx.User.Mention + " Ok");
        //                    var t = Task.Delay(DateTime.Now.AddHours(hh).AddMinutes(mm) - DateTime.Now);
        //                    t.Wait();
        //                    await ctx.RespondAsync(name + autor + " напомнить:\n**" + text + "**");
        //                }
        //                else await ctx.RespondAsync(ctx.User.Mention + " Выбери промежуток поменьше");
        //            }
        //            else await ctx.RespondAsync(ctx.User.Mention + " Время указывается в 24-x часовом формате ЧЧ:ММ (пример 13:30)");
        //        }
        //        else throw new Exception("Ошибка аргумента предлога (через/в) команды ");
        //    }
        //    else throw new Exception("Ошибка аргументов команды ");
        //}

        //#endregion
    }
}