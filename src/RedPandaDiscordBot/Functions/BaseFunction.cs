﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions
{
    /// <summary>
    /// Базовое представление любой функции, которую выполняют команды пользователя.
    /// Доступ к функциям осуществляется через ProviderFunction
    /// </summary>
    public abstract class BaseFunction
    {
        /// <summary>
        /// Локализированное имя функции
        /// </summary>
        public string NameLocal { get; protected set; }

        /// <summary>
        /// Локализированная группа 
        /// </summary>
        public string Group { get; protected set; }

        /// <summary>
        /// Локализированное описание
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// Тип функции
        /// </summary>
        public FuncType TypeFunc { get; protected set; }

        /// <summary>
        /// Индексы взаимоиспользуемых входных параметров
        /// (к примеру если обязательно нужен любой из двух параметров,
        /// то наличие одного из них отменяет обязательность второго и наоборот)
        /// </summary>
        public int[] Interchangeable { get; protected set; }

        /// <summary>
        /// Обязательное заполнение параметров в соответствующем порядке
        /// </summary>
        public bool[] RequiredProperties { get; protected set; }

        /// <summary>
        /// Описание входных параметров в соответствующем порядке
        /// </summary>
        public string[] DescriptionProperties { get; protected set; }

        /// <summary>
        /// Описание выходных результатов в соответствующем порядке
        /// </summary>
        public string[] DescriptionResults { get; protected set; }

        /// <summary>
        /// Типы свойств функции
        /// </summary>
        public Type[] TypeProperties { get; protected set; }

        /// <summary>
        /// Типы результатов функции
        /// </summary>
        public Type[] TypeResults { get; protected set; }

        /// <summary>
        /// Основная функция
        /// </summary>
        /// <param name="args">Аргументы различных типов</param>
        /// <returns>Результат различных типов</returns>
        public abstract object[] MainFunction(CommandContext ctx, object[] args);
    }
}