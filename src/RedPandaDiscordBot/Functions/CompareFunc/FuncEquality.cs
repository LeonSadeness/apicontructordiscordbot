﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Services;
using System;

namespace RedPandaDiscordBot.Functions.CompareFunc
{
    internal class FuncEquality : BaseFunction
    {
        public FuncEquality()
        {
            NameLocal = "Равенство";
            Group = "Операторы";
            Description = "Функция сравнения (==). Возращает равенство обьектов.";
            TypeFunc = FuncType.COMPARE;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true };
            DescriptionProperties = new string[] { "Операнд сравнения А", "Операнд сравнения B" };
            DescriptionResults = new string[] { "Результат сравнения в булевой переменной, где true - равны / false - не равны" };
            TypeProperties = new Type[] { typeof(object), typeof(object) };
            TypeResults = new Type[] { typeof(bool) };
        }

        public override object[] MainFunction(CommandContext _, object[] args)
        {
            if (args == null) throw new Exception("Массив аргументов пуст");
            dynamic a = args[0];
            dynamic b = args[1];

            try
            {
                return new object[] { a == b };
            }
            catch (Exception exp)
            {
                LoggerPanda.EventLogHandler("FuncEquality", exp);
            }

            return new object[] { false };
        }
    }
}