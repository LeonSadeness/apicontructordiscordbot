﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Services;
using System;

namespace RedPandaDiscordBot.Functions.CompareFunc
{
    internal class FuncLarger : BaseFunction
    {
        public FuncLarger()
        {
            NameLocal = "Больше";
            Group = "Операторы";
            Description = "Функция сравнения (>). Возращает отношение обьектов к друг другу, где первый больше второго.";
            TypeFunc = FuncType.COMPARE;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true };
            DescriptionProperties = new string[] { "Операнд сравнения А", "Операнд сравнения B" };
            DescriptionResults = new string[] { "Результат сравнения в булевой переменной, где true - операнд А больше операнда B / false - Операнд А меньше или равен операнду B" };
            TypeProperties = new Type[] { typeof(object), typeof(object) };
            TypeResults = new Type[] { typeof(bool) };
        }

        public override object[] MainFunction(CommandContext _, object[] args)
        {
            if (args == null) throw new Exception("Массив аргументов пуст");
            dynamic a = args[0];
            dynamic b = args[1];

            try
            {
                return new object[] { a > b };
            }
            catch (Exception exp)
            {
                LoggerPanda.EventLogHandler("FuncEquality", exp);
            }

            return new object[] { false };
        }
    }
}