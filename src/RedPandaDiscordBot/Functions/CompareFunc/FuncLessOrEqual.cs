﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Services;
using System;

namespace RedPandaDiscordBot.Functions.CompareFunc
{
    internal class FuncLessOrEqual : BaseFunction
    {
        public FuncLessOrEqual()
        {
            NameLocal = "Меньше или равно";
            Group = "Операторы";
            Description = "Функция сравнения (<=). Возращает отношение обьектов к друг другу, где первый меньше или равен второму.";
            TypeFunc = FuncType.COMPARE;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true };
            DescriptionProperties = new string[] { "Операнд сравнения А", "Операнд сравнения B" };
            DescriptionResults = new string[] { "Результат сравнения в булевой переменной, где true - операнд А меньше или равен операнду B / false - Операнд А больше операнда B" };
            TypeProperties = new Type[] { typeof(object), typeof(object) };
            TypeResults = new Type[] { typeof(bool) };
        }

        public override object[] MainFunction(CommandContext _, object[] args)
        {
            if (args == null) throw new Exception("Массив аргументов пуст");
            dynamic a = args[0];
            dynamic b = args[1];

            try
            {
                return new object[] { a <= b };
            }
            catch (Exception exp)
            {
                LoggerPanda.EventLogHandler("FuncEquality", exp);
            }

            return new object[] { false };
        }
    }
}