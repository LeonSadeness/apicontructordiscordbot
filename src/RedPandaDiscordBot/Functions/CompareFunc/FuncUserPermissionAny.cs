﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.PredicateFunc
{
    class FuncUserPermissionAny : BaseFunction
    {
        public FuncUserPermissionAny()
        {
            NameLocal = "Права пользователя";
            Group = "Функции Discord";
            Description = "Функция проверки разрешений пользователя по номерам.";
            TypeFunc = FuncType.COMPARE;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true };
            DescriptionProperties = new string[] { "Номера разрешений через пробел", "Участник сервера(гильдии)" };
            DescriptionResults = new string[] { "Наличие любого из указанных разрешений у пользователя" };
            TypeProperties = new Type[] { typeof(string), typeof(DiscordMember) };
            TypeResults = new Type[] { typeof(bool) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            if (args?[0] is string permissions)
            {
                DiscordMember member = args?.Length > 1 && args[1] is DiscordMember m ? m : ctx.Member;

                if (member.IsOwner)
                    return new object[] { true };

                foreach (var item in permissions.Split(' '))
                    if (Enum.TryParse(item, out DSharpPlus.Permissions num) && member.PermissionsIn(ctx.Channel).HasFlag(num))
                        return new object[] { true };

            }

            return new object[] { false };
        }
    }
}