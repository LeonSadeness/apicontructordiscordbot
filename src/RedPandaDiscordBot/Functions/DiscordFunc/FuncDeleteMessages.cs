﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;
using System.Collections.Generic;

namespace RedPandaDiscordBot.Functions.DiscordFunc
{
    class FuncDeleteMessages : BaseFunction
    {
        public FuncDeleteMessages()
        {
            NameLocal = "Удалить сообщение";
            Group = "Функции Discord";
            Description = "Удаляет сообщения в выбраном или текущем канале.";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { false, false };
            DescriptionProperties = new string[] { "Канал для удаления сообщений", "Кол-во сообщений" };
            DescriptionResults = null;
            TypeProperties = new Type[] { typeof(DiscordChannel), typeof(int) };
            TypeResults = null;
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            if (args == null) throw new Exception("Массив аргументов пуст");

            DiscordChannel chanel = args?[0] is DiscordChannel chl ? chl : ctx.Channel;

            if (args?[1] is int cnt && cnt > 0)
            {
                var messages = ctx.Channel.GetMessagesAsync(cnt).GetAwaiter().GetResult();
                ctx.Channel.DeleteMessagesAsync(messages);
            }
            else throw new Exception("Указанное кол-во сообщений для удаления нулевое");

            return null;
        }
    }
}