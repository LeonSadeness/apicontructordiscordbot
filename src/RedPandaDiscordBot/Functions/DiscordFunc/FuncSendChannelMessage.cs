﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.DiscordFunc
{
    internal class FuncSendChannelMessage : BaseFunction
    {
        public FuncSendChannelMessage()
        {
            NameLocal = "Сообщение в чат";
            Group = "Функции Discord";
            Description = "Отправляет сообщение на канал. Если канал отсутствует, отправляет сообщение на канал, где была вызвана команда";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = new int[] { 1, 2 };
            RequiredProperties = new bool[] { false, true, true };
            DescriptionProperties = new string[] { "Канал для отправки сообщения", "Текст сообщения", "Рамка оформления Embed" };
            DescriptionResults = null;
            TypeProperties = new Type[] { typeof(DiscordChannel), typeof(string), typeof(DiscordEmbed) };
            TypeResults = null;
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            if (args == null || args.Length < 1) throw new Exception("Массив аргументов пуст");

            DiscordChannel channel = (args[0] != null && args[0] is DiscordChannel chnl) ? chnl : ctx.Channel;
            string text = args.Length > 1 && args[1] is string txt ? txt : null;
            if (!string.IsNullOrWhiteSpace(text) || args[2] is DiscordEmbed)
                ctx.Client.SendMessageAsync(channel, text, embed: args[2] is DiscordEmbed de ? de : null);

            return null;
        }
    }
}