﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.DiscordFunc
{
    internal class FuncSendMessageMember : BaseFunction
    {
        public FuncSendMessageMember()
        {
            NameLocal = "Сообщение в приват";
            Group = "Функции Discord";
            Description = "Отправляет сообщение пользователю сервера в приват.";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true };
            DescriptionProperties = new string[] { "Пользователь (участник сервера Member)", "Текст сообщения", "Рамка оформления Embed" };
            DescriptionResults = null;
            TypeProperties = new Type[] { typeof(DiscordMember), typeof(string), typeof(DiscordEmbed) };
            TypeResults = null;
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            if (args == null) throw new Exception("Массив аргументов пуст");
            if (args.Length > 0 && args[0] is DiscordMember member)
            {
                if (args[1] is string text && string.IsNullOrWhiteSpace(text))
                {
                    member.SendMessageAsync(text, embed: args[2] is DiscordEmbed t ? t : null);
                }
                else throw new Exception("FuncSendMessageMember", new Exception("Параметр args[1] не содержит текста сообщения"));
            }
            else throw new Exception("FuncSendMessageMember", new Exception("Параметр args[0] не удалось преобразовать в DiscordMember"));
            return null;
        }
    }
}