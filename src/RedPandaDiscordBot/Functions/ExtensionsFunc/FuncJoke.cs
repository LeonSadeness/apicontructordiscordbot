﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RedPandaDiscordBot.Functions.ExtensionsFunc
{
    class FuncJoke : BaseFunction
    {
        public FuncJoke()
        {
            NameLocal = "Шутка";
            Group = "Расширения";
            Description = "Выдает шутку, или анекдот в кол-ве до 3-х";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { false };
            DescriptionProperties = new string[] { "Кол-во шуток" };
            DescriptionResults = new string[] { "Текст шутки" };
            TypeProperties = new Type[] { typeof(int) };
            TypeResults = new Type[] { typeof(string) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            Task.Run(() => ctx.TriggerTypingAsync());
            int count = args?[0] is int c && c > 0 ? c : 1;

            //if(args?[0] != null)

            // Адрес ресурса, к которому выполняется запрос
            //string url = "http://rzhunemogu.ru/Widzh/Anekdot2.aspx";
            //string url = "http://www.umori.li/api/random?num%3D1";
            //string url = "http://www.umori.li/api/get?site%3Dbash.im%26name%3Dbash%26num%3D100";
            string url_1 = "https://nekdo.ru/random/";

            using (var client = new WebClient())
            {
                string html = client.DownloadString(url_1);
                var match = Regex.Matches(html, @"(?<=id=""\d+"">)(?!\S*\<)(.+)(?=<\/)", RegexOptions.ECMAScript);
                string result = "";
                if (count > 3) count = 3;

                for (int i = 0; i < count; i++)
                    result += $"{match[i].Value}\n\n";

                return new object[] { result.Replace("<br>", Environment.NewLine) };
            }
        }
    }
}