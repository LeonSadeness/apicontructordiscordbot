﻿using Newtonsoft.Json.Linq;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Services;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedPandaDiscordBot.Functions.ExtensionsFunc
{
    class FuncTranslate : BaseFunction
    {
        public FuncTranslate()
        {
            NameLocal = "Переводчик";
            Group = "Расширения";
            Description = "Перводит текст на русский язык";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { true };
            DescriptionProperties = new string[] { "Текст для перевода" };
            DescriptionResults = new string[] { "Переведенный текст" };
            TypeProperties = new Type[] { typeof(string) };
            TypeResults = new Type[] { typeof(string) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            try
            {
                if (args.Length > 0 && args[0] is string text)
                {
                    Task.Run(() => ctx.TriggerTypingAsync());
                    string result = null;

                    if (string.IsNullOrWhiteSpace(Settings.Configuration["ya_translate_token"]))
                    {
                        Task.Run(() => ctx.RespondAsync("В конфигурации отсутствует ключ от Yandex Translate API\nПолучите ключ на https://translate.yandex.ru/developers/keys \nИ вставте значение в поле \"ya_translate_token\" конфигурационного файла"));
                        throw new Exception("В конфигурации отсутствует ключ от Yandex Translate API\nПолучите ключ на https://translate.yandex.ru/developers/keys \nИ вставте значение в поле \"ya_translate_token\" конфигурационного файла");
                    }

                    var token = Settings.Configuration["ya_translate_token"];
                    string url = $"https://translate.yandex.net/api/v1.5/tr.json/translate?key={token}&text={text}&lang=ru&format=plain";

                    using (var webClient = new WebClient())
                    {
                        string response = webClient.DownloadString(url);

                        JObject obj = JObject.Parse(response);
                        if (obj["code"].Value<int>() != 200)
                            throw new Exception("Ошибка - " + obj["code"].Value<int>());

                        result = ctx.Message.Author.Mention;
                        result += " Yandex Translate **" + obj["lang"].ToString().Substring(0, 2).ToUpper() + "-RU**\n";
                        result += obj["text"][0];
                    }
                    return new object[] { result };
                }
                else
                    throw new Exception("Ошибка аргументов команды");
            }
            catch
            {
                return new object[] { null };
                throw;
            }
        }
    }
}