﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedPandaDiscordBot.Functions
{
    public enum FuncType
    {
        /// <summary>
        /// Тип функции которая выполняет обычную операцию
        /// с неизвестным кол-вом входных и выходных данных
        /// </summary>
        OPERATION,
        /// <summary>
        /// Операция сравнения, принимает два параметра и возращает булево значение.
        /// </summary>
        COMPARE,
        /// <summary>
        /// Тип функции которая принимает один параметр и возвращает булево значение.
        /// Такой тип обычно высказывает решение бинарно решение на основе обьекта.
        /// </summary>
        PREDICATE
    }
}