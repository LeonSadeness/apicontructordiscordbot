﻿using Newtonsoft.Json.Linq;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace RedPandaDiscordBot.Functions
{
    /// <summary>
    /// Провайдер функций. Содержит список и методы работы со списком коммандных функций для CommandPanda
    /// </summary>
    public class FunctionProvider
    {
        /// <summary>
        /// Список коммандных функций для CommandPanda
        /// </summary>
        public static Dictionary<string, BaseFunction> AllFunction { get; private set; }

        static FunctionProvider()
        {
            AllFunction = new Dictionary<string, BaseFunction>();

            foreach (var item in Assembly.GetExecutingAssembly().DefinedTypes.Where(t => t.IsSubclassOf(typeof(BaseFunction))))
            {
                var obj = (BaseFunction)Activator.CreateInstance(item);
                AllFunction.Add(item.Name, obj);
            }
        }

        /// <summary>
        /// В случае нахождения функции по имени, предоставляет её основной метод.
        /// </summary>
        /// <param name="funcName">Имя функции</param>
        /// <param name="method">Метод, найденной функции</param>
        /// <returns>Успех поиска функции</returns>
        public static bool GetMainMethodFunc(string funcName, out Func<CommandContext, object[], object[]> method)
        {
            method = null;
            if (AllFunction.ContainsKey(funcName))
            {
                method = AllFunction[funcName].MainFunction;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Принимает метод для извлечения имени из типа в нужном формате, и
        /// возвращает описание всех функций в Json строке.
        /// </summary>
        /// <param name="stringTypeProvider">метод для извлечения имени типа</param>
        /// <returns>Json строка</returns>
        public static string GetDescriptionFuncsJson(Func<Type, string> stringTypeProvider)
        {
            if (Settings.Env.ToLower().StartsWith("dev"))
            {
                string json = "";
                if (AllFunction != null && AllFunction.Count > 0)
                {
                    JArray jArray = new JArray(
                        from func in AllFunction
                        select new JObject(
                        new JProperty("NameClass", func.Key),
                        new JProperty("NameLocal", func.Value.NameLocal),
                        new JProperty("Group", func.Value.Group),
                        new JProperty("Description", func.Value.Description),
                        new JProperty("TypeFunc", func.Value.TypeFunc.ToString()),
                        new JProperty("Interchangeable", func.Value.Interchangeable ?? new int[0]),
                        new JProperty("RequiredProperties", func.Value.RequiredProperties ?? new bool[0]),
                        new JProperty("DescriptionProperties", func.Value.DescriptionProperties ?? new string[0]),
                        new JProperty("DescriptionResults", func.Value.DescriptionResults ?? new string[0]),
                        new JProperty("TypeProperties", func.Value.TypeProperties != null ? func.Value.TypeProperties.Select(t => stringTypeProvider(t)) : new string[0]),
                        new JProperty("TypeResults", func.Value.TypeResults != null ? func.Value.TypeResults.Select(t => stringTypeProvider(t)) : new string[0])
                    ));
                    json = jArray.ToString(Newtonsoft.Json.Formatting.Indented);
                }
                return json;
            }
            else
            {
                return $"Функции разработчика запрещены в текущем окружении - \"{Settings.Env}\"";
            }
        }
    }
}