﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.PredicateFunc
{
    internal class FuncUserIsBot : BaseFunction
    {
        public FuncUserIsBot()
        {
            NameLocal = "Бот?";
            Group = "Функции Discord";
            Description = "Функция проверки. Возращает истинность принадлежности пользователя к боту";
            TypeFunc = FuncType.PREDICATE;
            Interchangeable = null;
            RequiredProperties = new bool[] { true };
            DescriptionProperties = new string[] { "Имя пользователя" };
            DescriptionResults = new string[] { "Результат принадлежности пользователя к боту, где true - бот / false - пользователь" };
            TypeProperties = new Type[] { typeof(DiscordUser) };
            TypeResults = new Type[] { typeof(bool) };
        }

        public override object[] MainFunction(CommandContext _, object[] args)
        {
            if (args == null) throw new Exception("Массив аргументов пуст");

            if (args[0] is DiscordUser user)
            {
                return new object[] { user.IsBot };
            }
            else throw new Exception("Не удалось преобразовать args[0] в DiscordUser");
        }
    }
}