﻿using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.PrivateFunc
{
    internal class FuncBuildCommand : BaseFunction
    {
        public FuncBuildCommand()
        {
            NameLocal = "Сборка команды";
            Group = "Приватные функции";
            Description = "Собирает команду на основе Json строки.";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { true };
            DescriptionProperties = new string[] { "Json команда" };
            DescriptionResults = new string[] { "Собранные комманды", "Статус сборки команды" };
            TypeProperties = new Type[] { typeof(string) };
            TypeResults = new Type[] {typeof(Command[]), typeof(string) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            // результат операции
            string result = "";
            // окончание в зависимости от кол-ва команд
            string endStr = "ы";

            // чистим строку
            string json = ((string)args?[0]?? "").Trim(new char[] { ' ', '\n', '\r', '\\', 'r', 'n', '`', 'j', 's', 'o', 'n' });
            // упаковываем в массив если он не таковой
            if(json.StartsWith("{"))
            {
                json = $"[{Environment.NewLine}{json}{Environment.NewLine}]";
                endStr = "a";
            }
            // сборка команд
            var cmds = ctx.CommandsPanda.BuildCommands(json, out string logB);
            result += logB;

            // возрат результата
            return new object[] {cmds.ToArray(), result == "" ? $"Команд{endStr} собран{endStr}" : $"Сборка не удалась:{Environment.NewLine}{result}" };
        }
    }
}