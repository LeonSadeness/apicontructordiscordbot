﻿using DSharpPlus.Entities;
using Newtonsoft.Json;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedPandaDiscordBot.Functions.PrivateFunc
{
    class FuncConfig : BaseFunction
    {
        public FuncConfig()
        {
            NameLocal = "Конфигурация";
            Group = "Приватные функции";
            Description = "Возращает загруженную конфигурацию, или отдельный параметр по ключу.";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { false };
            DescriptionProperties = new string[] { "Ключ параметра конфигурации" };
            DescriptionResults = new string[] { "Загруженная кофигурация бота в виде Json", "Загруженная конфигурация бота в рамке" };
            TypeProperties = new Type[] { typeof(string) };
            TypeResults = new Type[] { typeof(string), typeof(DiscordEmbed) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            Task.Run(() => ctx.TriggerTypingAsync());

            var conf = JsonConvert.DeserializeObject<Dictionary<string, string>>(Settings.GetActualConfigJson());
            string result1 = null;

            DiscordEmbedBuilder deb = new DiscordEmbedBuilder();
            deb.Footer = new DiscordEmbedBuilder.EmbedFooter() { Text = "RedPandas 2019", IconUrl = Settings.Configuration.ContainsKey("panda_icon_url")? Settings.Configuration["panda_icon_url"] : null };
            deb.Color = new DiscordColor(228, 48, 36);
            deb.Title = null;
            deb.Description = "Текущая конфигурация";
            foreach (var item in conf)
                deb.AddField(item.Key, string.IsNullOrWhiteSpace(item.Value)? "null" : item.Value, true);
            DiscordEmbed result2 = deb.Build();

            if (args?[0] is string key && conf.TryGetValue(key, out string value))
                result1 = $"```json\n{{\n  \"{key}\": \"{value}\"\n}}\n```";
            else
            result1 = $"```json\n{Settings.GetActualConfigJson()}\n```";

            return new object[] { result1, result2 };
        }
    }
}