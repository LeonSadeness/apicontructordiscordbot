﻿using DSharpPlus.Entities;
using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;
using System.Linq;

namespace RedPandaDiscordBot.Functions.PrivateFunc
{
    internal class FuncDefaultHelp : BaseFunction
    {
        public FuncDefaultHelp()
        {
            NameLocal = "Команда помощи";
            Group = "Приватные функции";
            Description = "Выводит список всех комманд бота.";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { false };
            DescriptionProperties = new string[] { "Имя команды" };
            DescriptionResults = new string[] { "Рамка с Help информацией" };
            TypeProperties = new Type[] { typeof(string) };
            TypeResults = new Type[] { typeof(DiscordEmbed) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            // права запросившего команду, для сокрытия некоторых команд в справке
            bool permission = ctx.Member.IsOwner ||
                ctx.Member.PermissionsIn(ctx.Channel).HasFlag(DSharpPlus.Permissions.Administrator) ||
                ctx.Member.PermissionsIn(ctx.Channel).HasFlag(DSharpPlus.Permissions.BanMembers) ||
                ctx.Member.PermissionsIn(ctx.Channel).HasFlag(DSharpPlus.Permissions.KickMembers) ||
                ctx.Member.PermissionsIn(ctx.Channel).HasFlag(DSharpPlus.Permissions.MuteMembers) ||
                ctx.Member.PermissionsIn(ctx.Channel).HasFlag(DSharpPlus.Permissions.DeafenMembers) ||
                ctx.Member.PermissionsIn(ctx.Channel).HasFlag(DSharpPlus.Permissions.ManageRoles);

            DiscordEmbedBuilder deb = new DiscordEmbedBuilder();
            //deb.Footer = new DiscordEmbedBuilder.EmbedFooter
            //{
            //    Text = $"RedPandas 2019",
            //    IconUrl = Settings.Configuration.ContainsKey("panda_icon_url") ? Settings.Configuration["panda_icon_url"] : null                
            //};
            //deb.Author = new DiscordEmbedBuilder.EmbedAuthor
            //{
            //    Name = "RedPandas 2019",
            //    IconUrl = Settings.Configuration.ContainsKey("panda_icon_url") ? Settings.Configuration["panda_icon_url"] : null,
            //    Url = $"{Settings.Configuration["panda_site_url"]}"
            //};
            //deb.Url = $"{Settings.Configuration["panda_site_url"]}"; // Делает синей ссылкой Title
            //deb.ThumbnailUrl = Settings.Configuration.ContainsKey("panda_icon_url") ? Settings.Configuration["panda_icon_url"] : null; // Иконка справа небольшая
            deb.Color = new DiscordColor(228, 48, 36);

            // проверяем нужнали справка по конкретной команде или общая | ну права
            if (args?[0] is string commandName && ctx.CommandsPanda.RegisteredCommands.ContainsKey(commandName) && permission)
            {
                // сама команда
                var cmd = ctx.CommandsPanda.RegisteredCommands[commandName];
                // имя команды
                var alias = cmd.Aliases?.Count > 0 ? " | " + string.Join(" | ", cmd.Aliases) : "";
                deb.Title = $"Help - {cmd.Name}{alias}";
                // её описание
                deb.Description = cmd.Description;

                // если есть Аргументы
                if (cmd.Arguments?.Count > 0)
                {
                    var decription = "";
                    for (int i = 0; i < cmd.Arguments.Count; i++)
                    {
                        decription += i == 0? "": Environment.NewLine;
                        // имя и описание
                        decription += $"**{cmd.Arguments[i].Name}** - {cmd.Arguments[i].Description}{Environment.NewLine}";
                        // тип
                        decription += $"**Тип аргумента** - {cmd.Arguments[i].Type}{Environment.NewLine}";
                        // свойста
                        var pref = (cmd.Arguments[i].IsOptional ? "Необязательный | " : "Обязательный | ") +
                            (cmd.Arguments[i].IsCatchAll ? "Перехватывает " : "Не перехватывает ") + "аргументы после себя" +
                            (cmd.Arguments[i].IsArray ? " | Массив" : "");
                        decription += $"**Свойства** - {pref}";
                        decription += i == cmd.Arguments.Count - 1 ? "" : Environment.NewLine + "______";
                    }
                    deb.AddField("Аргументы команды:", decription);
                }
            }
            else
            {
                deb.Title = "Help";
                //deb.Description = $"[RedPandas 2019]({Settings.Configuration["panda_site_url"]}){Environment.NewLine}";
                deb.Description = $"Введите help <имя команды>, чтобы узнать информацию о ней.{Environment.NewLine}{Environment.NewLine}";
                var cmds_Alfa = ctx.CommandsPanda.RegisteredCommands.Select(cmd => cmd.Value).Where(n => n.Name.ToLower() != "help").Distinct();
                // права на чтение скрытых команд
                Command[] cmds = permission ? cmds_Alfa.ToArray() : cmds_Alfa.Where(n => !n.IsHidden).ToArray();

                for (int i = 0; i < cmds.Length; i++)
                {
                    var alias = cmds[i].Aliases?.Count > 0 ? $" | `{string.Join("` | `", cmds[i].Aliases)}`" : "";
                    var endCmd = i == cmds.Length - 1 ? "" : Environment.NewLine;
                    deb.Description += $"`{cmds[i].Name}`{alias}";
                    deb.Description += $" - {cmds[i].Description}{endCmd}";
                }

                //deb.Description += $"{Environment.NewLine}{Environment.NewLine}[RedPandas 2019]({Settings.Configuration["panda_icon_url"]})";
            }

            DiscordEmbed discordEmbed = deb.Build();
            return new object[] { discordEmbed };
        }
    }
}