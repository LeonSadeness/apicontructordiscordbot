﻿using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RedPandaDiscordBot.Functions.PrivateFunc
{
    class FuncFindCommand : BaseFunction
    {
        public FuncFindCommand()
        {
            NameLocal = "Поиск команды";
            Group = "Приватные функции";
            Description = "Находит указанные в массиве комманды, и передает их обьекты если они существуют";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { false };
            DescriptionProperties = new string[] { "Имена команд" };
            DescriptionResults = new string[] { "Найденные комманды" };
            TypeProperties = new Type[] { typeof(string[]) };
            TypeResults = new Type[] { typeof(Command[]) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            List<Command> cmds = null;

            if (args?[0] is string[] namesCommand && namesCommand.Length > 0)
            {
                cmds = new List<Command>();
                foreach (var item in namesCommand)
                    if (ctx.CommandsPanda.RegisteredCommands.ContainsKey(item.ToLower()))
                        cmds.Add(ctx.CommandsPanda.RegisteredCommands[item]);
            }

            return new object[] { cmds.Distinct().ToArray() };
        }
    }
}