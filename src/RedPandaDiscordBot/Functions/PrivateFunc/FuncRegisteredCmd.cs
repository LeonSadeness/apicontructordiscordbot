﻿using RedPandaDiscordBot.CPandaModule.Cmd;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.PrivateFunc
{
    class FuncRegisteredCmd : BaseFunction
    {
        public FuncRegisteredCmd()
        {
            NameLocal = "Регистрация команды";
            Group = "Приватные функции";
            Description = "Регистрирует или удаляет команду из бота.";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true };
            DescriptionProperties = new string[] { "Команды для регистрации или удаления", "Регистрация команды - true/ Удаление команды - false" };
            DescriptionResults = new string[] { "Зарегестрированные комманды", "Статус сборки команды" };
            TypeProperties = new Type[] { typeof(Command[]), typeof(bool) };
            TypeResults = new Type[] { typeof(Command[]), typeof(string) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            string result = "Нет обьектов для регистрации/удаления";
            string endStr = "ы";
            bool reg = (args?[1] is bool r) ? r : true;
            Command[] cmds = args?[0] is Command[] c ? c : null;

            if (cmds?.Length > 0)
            {
                result = "";
                endStr = cmds.Length > 1 ? "ы" : "а";

                foreach (var item in cmds)
                {
                    if (reg)
                    {
                        ctx.CommandsPanda.RegisterCommands(item, out string logR);
                        result += logR==""? "" : logR + Environment.NewLine;
                    }
                    else
                    {
                        ctx.CommandsPanda.UnregisteredCommand(item, out string logR);
                        result += logR == "" ? "" : logR + Environment.NewLine;
                    }
                }
            }
            string strReg = reg ? "регестрирован" : "удален";
            string prefix = reg ? "за" : "";
            return new object[] { cmds, result == "" ? $"Команд{endStr} {prefix}{strReg}{endStr}" : $"Ошибка при {strReg}ии:{Environment.NewLine}{result}" };
        }
    }
}