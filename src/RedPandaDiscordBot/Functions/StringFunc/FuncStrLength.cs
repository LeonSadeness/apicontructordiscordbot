﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.StringFunc
{
    class FuncStrLength : BaseFunction
    {
        public FuncStrLength()
        {
            NameLocal = "Длинна строки";
            Group = "Функции строк";
            Description = "Вычисляет длинну строки";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { true };
            DescriptionProperties = new string[] { "Текст строки" };
            DescriptionResults = new string[] { "Длинна строки" };
            TypeProperties = new Type[] { typeof(string) };
            TypeResults = new Type[] { typeof(int) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            int result = 0;

            if (args?[0] is string text)
                result = text.Length;

            return new object[] { result };
        }
    }
}