﻿using RedPandaDiscordBot.CPandaModule.EventArgs;
using System;

namespace RedPandaDiscordBot.Functions.StringFunc
{
    class FuncStringContact : BaseFunction
    {
        public FuncStringContact()
        {
            NameLocal = "Соединить строки";
            Group = "Функции строк";
            Description = "Соединяет две строки. Если установлен параметр True, то соединение идет через символ новой строки";
            TypeFunc = FuncType.OPERATION;
            Interchangeable = null;
            RequiredProperties = new bool[] { true, true, false };
            DescriptionProperties = new string[] { "Строка A", "Строка В", "Вставить символ новой строки в соединении" };
            DescriptionResults = new string[] { "Соединенная строка" };
            TypeProperties = new Type[] { typeof(string), typeof(string), typeof(bool) };
            TypeResults = new Type[] { typeof(string) };
        }

        public override object[] MainFunction(CommandContext ctx, object[] args)
        {
            string result = null;
            bool newLine = args?[2] is bool n ? n : false;

            if (args?[0] is string A && args?[1] is string B)
            {
                result = newLine ? A + Environment.NewLine + B : A + B;
            }

            return new object[] { result };
        }
    }
}