﻿using DSharpPlus;
using DSharpPlus.EventArgs;
using RedPandaDiscordBot.CBuilder;
using RedPandaDiscordBot.CBuilder.EventArgs;
using RedPandaDiscordBot.CPandaModule;
using RedPandaDiscordBot.CPandaModule.EventArgs;
using RedPandaDiscordBot.CPandaModule.Extensions;
using RedPandaDiscordBot.CPandaModule.Interfaces;
using RedPandaDiscordBot.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedPandaDiscordBot
{
    class RedPandaClient
    {
        public DiscordClient Client { get; set; }
        public CommandPandaModule Commands { get; set; }

        public static void Main(string[] args)
        {
            LoggerPanda.SessionName = Settings.SessionName;

            var prog = new RedPandaClient();
            prog.RunBotAsync().GetAwaiter().GetResult();
        }

        public async Task RunBotAsync()
        {
            var cfg = new DiscordConfiguration
            {
                Token = Settings.BotToken,
                TokenType = TokenType.Bot,

                AutoReconnect = true,
                LogLevel = LogLevel.Debug,
                UseInternalLogHandler = true
            };

            Client = new DiscordClient(cfg);

            // Далее, давайте подключим некоторые события, чтобы мы знали что происходит
            Client.Ready += Client_Ready;
            Client.GuildAvailable += Client_GuildAvailable;
            Client.ClientErrored += Client_ClientError;

            // Создадим билдеры команд
            var builders = new List<ICommandBuilder>() { new BaseCommandBuilder() };

            // Далее, давайте настроим наши команды
            CommandsPandaConfiguration confCmd = new CommandsPandaConfiguration
            {
                // Используем префикс строки, определенный в config.json
                StringPrefix = Settings.MsgPrefix,

                // Включим ответ в прямых сообщениях
                EnablePM = true,

                // Включим упоминание бота в качестве префикса команды
                EnableMentionPrefix = true,
                EnableDefaultHelp = true,

                // Добавим билдеры комманд
                Builders = builders
            };

            // и подключим их
            Commands = Client.UseCommandsPanda(confCmd);

            // давайте подключим некоторые командные события, чтобы мы знали, что происходит
            Commands.CommandExecuted += Commands_CommandExecuted;
            Commands.CommandErrored += Commands_CommandErrored;

            // Добавим список стандартных команд
            foreach (var item in DefaultCommandPanda.Commands)
                Commands.RegisterCommands(item);

            // Наконец, давайте подключиться к серверу
            await Client.ConnectAsync();

            // Для бесконечного ожидания
            await Task.Delay(-1);
        }

        #region Handlers

        private Task Client_Ready(ReadyEventArgs e)
        {
            // let's log the fact that this event occured
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "ExampleBot", "Client is ready to process events.", DateTime.Now);

            

            // так как этот метод не асинхронный, давайте вернемся
            // выполненное задание, так что никакой дополнительной работы
            // готово
            return Task.CompletedTask;
        }

        private Task Client_GuildAvailable(GuildCreateEventArgs e)
        {
            // let's log the name of the guild that was just
            // sent to our client
            e.Client.DebugLogger.LogMessage(LogLevel.Info, "ExampleBot", $"Guild available: {e.Guild.Name}", DateTime.Now);

            // since this method is not async, let's return
            // a completed task, so that no additional work
            // is done
            return Task.CompletedTask;
        }

        private Task Client_ClientError(ClientErrorEventArgs e)
        {
            // let's log the details of the error that just 
            // occured in our client
            e.Client.DebugLogger.LogMessage(LogLevel.Error, "ExampleBot", $"Exception occured: {e.Exception.GetType()}: {e.Exception.Message}", DateTime.Now);

            // since this method is not async, let's return
            // a completed task, so that no additional work
            // is done
            return Task.CompletedTask;
        }

        private Task Commands_CommandExecuted(CommandExecutionEventArgs e)
        {
            // let's log the name of the command and user
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Info, "ExampleBot", $"{e.Context.User.Username} successfully executed '{e.Command.Name}'", DateTime.Now);

            // since this method is not async, let's return
            // a completed task, so that no additional work
            // is done
            return Task.CompletedTask;
        }

        private async Task Commands_CommandErrored(CommandErrorEventArgs e)
        {
            await Task.Run(() => LoggerPanda.EventLogHandler("Command - " + e?.Command?.Name ?? "NoName", e?.Exception ?? new Exception("Команда не найдена")));

            //// let's log the error details
            //e.Context.Client.DebugLogger.LogMessage(LogLevel.Error, "ExampleBot", $"{e.Context.User.Username} tried executing '{e.Command?.QualifiedName ?? "<unknown command>"}' but it errored: {e.Exception.GetType()}: {e.Exception.Message ?? "<no message>"}", DateTime.Now);

            //// let's check if the error is a result of lack
            //// of required permissions
            //if (e.Exception is ChecksFailedException ex)
            //{
            //    // yes, the user lacks required permissions, 
            //    // let them know

            //    var emoji = DiscordEmoji.FromName(e.Context.Client, ":no_entry:");

            //    // let's wrap the response into an embed
            //    var embed = new DiscordEmbedBuilder
            //    {
            //        Title = "Access denied",
            //        Description = $"{emoji} You do not have the permissions required to execute this command.",
            //        Color = new DiscordColor(0xFF0000) // red
            //        // there are also some pre-defined colors available
            //        // as static members of the DiscordColor struct
            //    };
            //    await e.Context.RespondAsync("", embed: embed);
            //}
        }

        private async Task BuilderErrorHandler(System.EventArgs e)
        {
            await Task.Run(() => Client.DebugLogger.LogMessage(LogLevel.Error, "Builder", $"Ошибка при сборке: {((BuilderEventArgs)e).Msg}", DateTime.Now));
        }

        #endregion
    }
}