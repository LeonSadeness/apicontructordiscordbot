﻿using System;
using System.IO;
using System.Text;

namespace RedPandaDiscordBot.Services
{
    internal static class LoggerPanda
    {
        /// <summary>
        /// Имя сессии. Используется в качестве имени лог файла. По умолчанию NoName
        /// </summary>
        public static string SessionName { get; internal set; } = "NoName";

        static LoggerPanda()
        {
            if (!Directory.Exists(Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Logs"))
            {
                Directory.CreateDirectory(Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Logs");
            }
        }
        public static void EventLogHandler(string event_name, Exception ex)
        {
            ConsoleLog(event_name, ex);
            FileLog(event_name, ex);
        }

        public static void ConsoleLog(string event_name, Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write($"[{DateTime.Now}] {event_name.ToUpper()}: ");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{ex.Message}");
            Console.ResetColor();
        }

        static object _locker = new object();

        static void FileLog(string event_name, Exception ex)
        {
            lock (_locker)
            {
                if (SessionName == "NoName" || string.IsNullOrWhiteSpace(SessionName))
                    ConsoleLog("LOGGER", new Exception("Имя сессии не установлено. Укажите имя сессии в LoggerPanda.SessionName"));

                using (StreamWriter tw = new StreamWriter(Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Logs" + Path.DirectorySeparatorChar + SessionName + ".txt", true, Encoding.Default))
                {
                    tw.Write($"[{DateTime.Now}] {event_name.ToUpper()}: ");
                    tw.WriteLine($"{ex.Message}");
                }
            }
        }
    }
}
