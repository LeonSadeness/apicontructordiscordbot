﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RedPandaDiscordBot.CPandaModule.Extensions;
using RedPandaDiscordBot.Functions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RedPandaDiscordBot.Services
{
    /// <summary>
    /// Класс настроек бота. Реализует чтение настроек из конфигурационного файла и инкапсуляцию полей настроек RedPandasDiscordBot.
    /// </summary>
    public class Settings
    {
        #region Main Properties

        /// <summary>
        /// Название текущей сессии. Устанавливается при старте в зависмости от конфигурации и времени.
        /// </summary>
        public static string SessionName { get; private set; }

        /// <summary>
        /// Локализация
        /// </summary>
        public static string Lang { get; private set; }

        /// <summary>
        /// Окружение
        /// </summary>
        public static string Env { get; private set; }

        /// <summary>
        /// Токен бота для регистрации в сервисах Discord
        /// </summary>
        public static string BotToken { get; private set; }

        /// <summary>
        /// Префикс сообщений на которые реагирует бот без прямого обращения
        /// </summary>
        public static string MsgPrefix { get; private set; }

        /// <summary>
        /// Конфигурационный класс Json
        /// </summary>
        public static Dictionary<string, string> Configuration;

        #endregion

        #region SQL Property

        /// <summary>
        /// Хост SQL
        /// </summary>
        public static string SQLHost => Configuration["mysql_server"];

        /// <summary>
        /// Порт SQL
        /// </summary>
        public static string SQLPort => Configuration["mysql_port"];

        /// <summary>
        /// Имя пользователя SQL
        /// </summary>
        public static string SQLUsername => Configuration["mysql_user"];

        /// <summary>
        /// Пароль SQL
        /// </summary>
        public static string SQLPassword => Configuration["mysql_password"];

        /// <summary>
        /// База SQL
        /// </summary>
        public static string SQLDatabase => Configuration["mysql_db"];

        /// <summary>
        /// Строка подключения для SQL
        /// </summary>
        /// <returns></returns>
        public static string GetSQLConnectString()
        {
            return $"Server = {SQLHost}; Database = {SQLDatabase}; Port = {SQLPort}; Username = {SQLUsername}; Password = {SQLPassword};";
        }

        #endregion

        /// <summary>
        /// Конструкктор этого класса. Создает конфигурацию из Json файлов.
        /// </summary>
        static Settings()
        {
            SessionName = $"{Env}_{DateTime.Now.ToString("dd.MM.yyyy_hh.mm")}";
            Configuration = new Dictionary<string, string>();

#if DEBUG
            if (!Directory.Exists(Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Configs"))
            {
                throw new Exception("Отсутствует дирректория - \"" + Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Configs\"");
            }

            string[] pathsConfig = Directory.GetFiles(Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Configs", "*.json", SearchOption.TopDirectoryOnly);
            if (pathsConfig.Length == 0)
            {
                throw new Exception("Отсутствует файл конфигурации в дирректории - \"" + Environment.CurrentDirectory + Path.DirectorySeparatorChar + "Configs\"");
            }

            foreach (string path in pathsConfig)
            {
                string json = "";
                using (StreamReader sr = new StreamReader(path))
                    json = sr.ReadToEnd();
                var cfg = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
                foreach (var item in cfg)
                    Configuration[item.Key] = item.Value;
            }

            // Загружает config.json в debag режиме, заменяя значения при совпадении ключей.
            string jsonLast = "";
            using (StreamReader sr = new StreamReader($"Configs{Path.DirectorySeparatorChar}config.json"))
                jsonLast = sr.ReadToEnd();
            var cfgLast = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonLast);
            foreach (var item in cfgLast)
                Configuration[item.Key] = item.Value;
#else
            var k = Environment.GetEnvironmentVariables().Keys.Cast<string>().ToArray();
            var v = Environment.GetEnvironmentVariables().Values.Cast<string>().ToArray(); ;
            Configuration = new Dictionary<string, string>();
            for (int i = 0; i < k.Length; i++)
                Configuration[k[i]] = v[i] ?? "";
#endif
            Env = Configuration["env"];
            BotToken = Configuration["bot_token"];
            MsgPrefix = Configuration["msg_prefix"];

            Lang = Configuration.TryGetValue("lang", out string value) && !string.IsNullOrWhiteSpace(value) ? value : "ru" ;
            
        }

        /// <summary>
        /// Возвращает текущую собраную конфигурацию в формате Json в строковом представлении
        /// </summary>
        /// <param name="hideToken">Скрывать токен (стандартное значение True)</param>
        /// <returns>Строка формата Json</returns>
        public static string GetActualConfigJson(bool hideToken = true)
        {
            string result = JsonConvert.SerializeObject(Configuration, Formatting.Indented);
            if (hideToken)
            {
                var tokens = Configuration.Where(c => c.Key.ToLower().Contains("token"));
                foreach (var item in tokens)
                {
                    string startValue = item.Value;
                    string endValue = "";

                    for (int j = 0; j < startValue.Length; j++)
                        endValue += (j > 5 && j < startValue.Length - 5) ? '*' : startValue[j];

                    result = result.Replace(startValue, endValue);
                }
            }
            return result;
        }
    }
}