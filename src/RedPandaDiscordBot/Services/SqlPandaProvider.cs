﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;

namespace RedPandaDiscordBot.Services
{
    internal class SqlPandaProvider
    {
        public static MySqlConnection SqlConnection { get; private set; }

        static SqlPandaProvider()
        {
            SqlConnection = new MySqlConnection(Settings.GetSQLConnectString());
            SqlConnection.Open();
        }

        /// <summary>
        /// Тестовый метод
        /// </summary>
        /// <param name="request">Sql запрос</param>
        /// <returns></returns>
        public static object[] ExecuteSqlScript(string request)
        {
            List<object> objects = new List<object>();

            using (MySqlCommand command = new MySqlCommand(request, SqlConnection))
            {
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    objects.Add(reader[0]);
                }
            }
            return objects.ToArray();
        }

        ~SqlPandaProvider()
        {
            SqlConnection.Close();
        }


    }
}
